package com.test;

import com.jayway.jsonpath.JsonPath;
import com.test.entity.game.Game;
import com.test.entity.game.player.Player;
import com.test.entity.game.player.field.cell.CellSnapshot;
import com.test.entity.game.player.field.placement.algorithm.random.PositionIterator;
import com.test.entity.game.player.field.placement.algorithm.random.ShipsPositionsGenerator;
import com.test.entity.game.player.field.placement.algorithm.specified.SpecifiedPlacement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.util.LinkedList;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Тест для генератора расположений кораблей, проверки работоспособности ресурсов и
 * прогона игры с предопределенными ходами игроков
 * Для теста используется предопределенное расположение кораблей
 *
 * @see SpecifiedPlacement
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SeaBattleApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
@DirtiesContext
@PropertySource("classpath:localize/message/message.properties")
public class SeaBattleTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SeaBattleTest.class); // Логгер

    @Value("${local.server.port}")
    private int port; // Порт

    // Сообщения с результатами хода
    @Value("${destroy-deck}")
    private String destroyDeck;
    @Value("${destroy-ship}")
    private String destroyShip;
    @Value("${miss}")
    private String miss;
    @Value("${win}")
    private String win;

    @Autowired
    private WebApplicationContext webApplicationContext; // Контекст приложения
    private MediaType contentType; // Тип содержимого
    private MockMvc mockMvc; // Входная точка для тестирования REST-сервисов

    /**
     * Инициализация перед запуском теста
     *
     * @throws Exception в случае какой-либо ошибки
     */
    @Before
    public void setup() throws Exception {
        contentType = MediaType.APPLICATION_JSON_UTF8;
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    /**
     * Проверка генератора данных для расположения кораблей на игровом поле
     */
    @Test
    @Timed(millis = 60000)
    public void generateByRandomAlgorithm() {
        // Цикл от 0 до 99
        for (int i = 0; i < 100; i++) {
            // Создание генератора
            final ShipsPositionsGenerator generator = new ShipsPositionsGenerator(10, 10);
            // Генерация и печать данных
            printShipsPositions(generator.generatePositions());
        }
    }

    /**
     * Печать сгенерированных данных для расположения кораблей на игровом поле
     *
     * @param shipsPositions {@link PositionIterator}
     */
    private void printShipsPositions(PositionIterator[] shipsPositions) {
        final String[][] field = new String[10][10];
        for (String[] row : field) {
            for (int i = 0; i < 10; i++) {
                row[i] = "~";
            }
        }
        PositionIterator ship;
        for (int i = 0; i < shipsPositions.length; i++) {
            ship = shipsPositions[i];
            if (ship.getAngle() == 0) {
                for (int j = ship.getColumn(); j < ship.getColumn() + ship.getSize(); j++) {
                    field[ship.getRow()][j] = String.valueOf(i);
                }
            } else {
                for (int j = ship.getRow(); j < ship.getRow() + ship.getSize(); j++) {
                    field[j][ship.getColumn()] = String.valueOf(i);
                }
            }
        }
        LOGGER.info("-----Placement-----");
        for (String[] row : field) {
            final StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 10; i++) {
                builder.append(row[i]);
            }
            LOGGER.info(builder.toString());
        }
    }

    /**
     * Проверка на возможность работы с ресурсами (игра, игроки, статус игрока, поле игрока, ячейки поля игрока)
     *
     * @throws Exception в случае возникновения какого-либо исключения
     */
    @Test
    public void checkResources() throws Exception {
        // Проверка авторизации (запросе без заголовка "player-name")
        LOGGER.info("Request without \"player-name\" header - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", ""))
                .andExpect(status().isUnauthorized());
        LOGGER.info("Request without \"player-name\" header - test successful completion");

        // Получение информации об игре игроком 1
        LOGGER.info("Player 1 requests game info - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Game.State.PLAYERS_IS_NOT_READY.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player1", not(hasKey("field"))))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player2", not(hasKey("field"))));
        LOGGER.info("Player 1 requests game info - test successful completion");

        // Получение информации об игре игроком 2
        LOGGER.info("Player 2 requests game info - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Game.State.PLAYERS_IS_NOT_READY.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player1", not(hasKey("field"))))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player2", not(hasKey("field"))));
        LOGGER.info("Player 2 requests game info - test successful completion");

        // Получение списка игроков игроком 1
        LOGGER.info("Player 1 requests list of players - test start");
        mockMvc.perform(get("/game/players")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].name", containsInAnyOrder("player1", "player2")))
                .andExpect(jsonPath("$.[*].state", everyItem(is(Player.State.IS_NOT_READY.name()))))
                .andExpect(jsonPath("$.[*]", everyItem(not(hasKey("field")))));
        LOGGER.info("Player 1 requests list of players - test successful completion");

        // Получение списка игроков игроком 2
        LOGGER.info("Player 2 requests list of players - test start");
        mockMvc.perform(get("/game/players")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[*].name", containsInAnyOrder("player1", "player2")))
                .andExpect(jsonPath("$.[*].state", everyItem(is(Player.State.IS_NOT_READY.name()))))
                .andExpect(jsonPath("$.[*]", everyItem(not(hasKey("field")))));
        LOGGER.info("Player 2 requests list of players - test successful completion");

        // Получение информации об игроке 1 игроком 1
        LOGGER.info("Player 1 requests player 1 info - test start");
        mockMvc.perform(get("/game/players/player1")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is("player1")))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$", not(hasKey("field"))));
        LOGGER.info("Player 1 requests player 1 info - test successful completion");

        // Получение информации об игроке 2 игроком 1
        LOGGER.info("Player 1 requests player 2 info - test start");
        mockMvc.perform(get("/game/players/player2")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is("player2")))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$", not(hasKey("field"))));
        LOGGER.info("Player 1 requests player 2 info - test successful completion");

        // Получение информации об игроке 2 игроком 2
        LOGGER.info("Player 2 requests player 2 info - test start");
        mockMvc.perform(get("/game/players/player2")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is("player2")))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$", not(hasKey("field"))));
        LOGGER.info("Player 2 requests player 2 info - test successful completion");

        // Получение информации об игроке 1 игроком 2
        LOGGER.info("Player 2 requests player 1 info - test start");
        mockMvc.perform(get("/game/players/player1")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.name", is("player1")))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$", not(hasKey("field"))));
        LOGGER.info("Player 2 requests player 1 info - test successful completion");

        // Получение игрового поля игрока 1 игроком 1
        LOGGER.info("Player 1 requests player 1 field - test start");
        mockMvc.perform(get("/game/players/player1/field")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isNotFound());
        LOGGER.info("Player 1 requests player 1 field - test successful completion");

        // Получение игрового поля игрока 2 игроком 2
        LOGGER.info("Player 2 requests player 2 field - test start");
        mockMvc.perform(get("/game/players/player2/field")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isNotFound());
        LOGGER.info("Player 2 requests player 2 field - test successful completion");

        // Получение статуса игрока 1 игроком 1
        LOGGER.info("Player 1 requests player 1 state - test start");
        mockMvc.perform(get("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())));
        LOGGER.info("Player 1 requests player 1 state - test successful completion");

        // Получение статуса игрока 2 игроком 1
        LOGGER.info("Player 1 requests player 2 state - test start");
        mockMvc.perform(get("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())));
        LOGGER.info("Player 1 requests player 2 state - test successful completion");

        // Получение статуса игрока 2 игроком 2
        LOGGER.info("Player 2 requests player 2 state - test start");
        mockMvc.perform(get("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())));
        LOGGER.info("Player 2 requests player 2 state - test successful completion");

        // Получение статуса игрока 1 игроком 2
        LOGGER.info("Player 2 requests player 1 state - test start");
        mockMvc.perform(get("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())));
        LOGGER.info("Player 2 requests player 1 state - test successful completion");

        // Изменение статуса игрока 1 с "Не готов к игре" на "Готов к игре" игроком 1 (успех)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Получение обновленного статуса игрока 1 ("Готов к игре") игроком 1
        LOGGER.info("Player 1 requests player 1 new state - test start");
        mockMvc.perform(get("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_READY.name())));
        LOGGER.info("Player 1 requests player 1 new state - test successful completion");

        // Изменение статуса игрока 1 с "Готов к игре" на "Не готов к игре" игроком 1 (успех)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_NOT_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Изменение статуса игрока 1 с "Не готов к игре" на "Играет" игроком 1 (ошибка)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_PLAYING.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Изменение статуса игрока 1 с "Не готов к игре" на "Готов к перезапуску игры" игроком 1 (ошибка)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY_FOR_RESTART.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Получение старого статуса игрока 1 игроком 1 (за предыдущие 2 запроса он не должен измениться)
        LOGGER.info("Player 1 requests player 1 new state - test start");
        mockMvc.perform(get("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_NOT_READY.name())));
        LOGGER.info("Player 1 requests player 1 new state - test successful completion");

        // Изменение статуса игрока 1 с "Не готов к игре" на "Готов к игре" игроком 1 (успех)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Получение обновленного статуса игрока 1 ("Готов к игре") игроком 1
        LOGGER.info("Player 1 requests player 1 new state - test start");
        mockMvc.perform(get("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_READY.name())));
        LOGGER.info("Player 1 requests player 1 new state - test successful completion");

        // Изменение статуса игрока 2 с "Не готов к игре" на "Готов к игре" игроком 1 (ошибка)
        LOGGER.info("Player 1 requests to changing player 2 state - test start");
        mockMvc.perform(put("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        LOGGER.info("Player 1 requests to changing player 2 state - test successful completion");

        // Изменение статуса игрока 2 с "Не готов к игре" на "Готов к игре" игроком 2
        LOGGER.info("Player 2 requests to changing player 2 state - test start");
        mockMvc.perform(put("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player2")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 2 requests to changing player 2 state - test successful completion");

        // Получение информации о начавшейся игре игроком 1
        LOGGER.info("Player 1 requests game info - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", isOneOf(
                        Game.State.PLAYER1_IS_TURNING.name(),
                        Game.State.PLAYER2_IS_TURNING.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player1", hasKey("field")))
                .andExpect(jsonPath("$.player1.field.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.player1.field.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.player1.field.cells[*][*].content", everyItem(isOneOf(
                        CellSnapshot.Content.DECK.name(),
                        CellSnapshot.Content.EMPTINESS.name()))))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player2", hasKey("field")))
                .andExpect(jsonPath("$.player2.field.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.player2.field.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.player2.field.cells[*][*].content", everyItem(
                        is(CellSnapshot.Content.SECRET.name()))));
        LOGGER.info("Player 1 requests game info - test successful completion");

        // Получение информации о начавшейся игре игроком 2
        LOGGER.info("Player 2 requests game info - test start");
        final ResultActions resultActions = mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", isOneOf(
                        Game.State.PLAYER1_IS_TURNING.name(),
                        Game.State.PLAYER2_IS_TURNING.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player1", hasKey("field")))
                .andExpect(jsonPath("$.player1.field.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.player1.field.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.player1.field.cells[*][*].content", everyItem(
                        is(CellSnapshot.Content.SECRET.name()))))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player2", hasKey("field")))
                .andExpect(jsonPath("$.player2.field.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.player2.field.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.player2.field.cells[*][*].content", everyItem(isOneOf(
                        CellSnapshot.Content.DECK.name(),
                        CellSnapshot.Content.EMPTINESS.name()))));
        // Тело ответа
        final String content = resultActions.andReturn().getResponse().getContentAsString();
        // Статус игры
        final String state = JsonPath.read(content, "$.state");
        // Флаг, указывающий на то, что ходит игрок 1 (иначе игрок 2)
        final boolean isPlayer1Turn = Game.State.PLAYER1_IS_TURNING.name().equals(state);
        LOGGER.info("Player 2 requests game info - test successful completion");

        // Изменение статуса игрока 1 с "Играет" на "Не готов к игре" игроком 1 (ошибка)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_NOT_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Изменение статуса игрока 1 с "Играет" на "Готов к игре" игроком 1 (ошибка)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Изменение статуса игрока 1 с "Играет" на "Готов перезапуску игры" игроком 1 (успех)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY_FOR_RESTART.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Получение обновленного статуса игрока 1 ("Готов перезапуску игры") игроком 1
        LOGGER.info("Player 1 requests player 1 new state - test start");
        mockMvc.perform(get("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(Player.State.IS_READY_FOR_RESTART.name())));
        LOGGER.info("Player 1 requests player 1 new state - test successful completion");

        // Получение поля игрока 1 игроком 1 (все ячейки известны)
        LOGGER.info("Player 1 requests player 1 field - test start");
        mockMvc.perform(get("/game/players/player1/field")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.cells[*][*].content", everyItem(isOneOf(
                        CellSnapshot.Content.DECK.name(),
                        CellSnapshot.Content.EMPTINESS.name()))));
        LOGGER.info("Player 1 requests player 1 field - test successful completion");

        // Получение поля игрока 2 игроком 2 (все ячейки известны)
        LOGGER.info("Player 2 requests player 2 field - test start");
        mockMvc.perform(get("/game/players/player2/field")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.cells[*][*].content", everyItem(isOneOf(
                        CellSnapshot.Content.DECK.name(),
                        CellSnapshot.Content.EMPTINESS.name()))));
        LOGGER.info("Player 2 requests player 2 field - test successful completion");

        // Получение поля игрока 2 игроком 1 (все ячейки засекречены)
        LOGGER.info("Player 1 requests player 2 field - test start");
        mockMvc.perform(get("/game/players/player2/field")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.cells[*][*].content", everyItem(
                        is(CellSnapshot.Content.SECRET.name()))));
        LOGGER.info("Player 1 requests player 2 field - test successful completion");

        // Получение поля игрока 1 игроком 2 (все ячейки засекречены)
        LOGGER.info("Player 2 requests player 1 field - test start");
        mockMvc.perform(get("/game/players/player1/field")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.cells[*]", hasSize(10)))
                .andExpect(jsonPath("$.cells[*]", everyItem(hasSize(10))))
                .andExpect(jsonPath("$.cells[*][*].content", everyItem(
                        is(CellSnapshot.Content.SECRET.name()))));
        LOGGER.info("Player 2 requests player 1 field - test successful completion");

        // Получение ячейки игрока 1 игроком 1 (палуба)
        LOGGER.info("Player 1 requests player 1 cell - test start");
        mockMvc.perform(get("/game/players/player1/field/cells/1a")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", is(CellSnapshot.Content.DECK.name())));
        LOGGER.info("Player 1 requests player 1 cell - test successful completion");

        // Получение ячейки игрока 2 игроком 1 (секрет)
        LOGGER.info("Player 1 requests player 2 cell - test start");
        mockMvc.perform(get("/game/players/player2/field/cells/1d")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", is(CellSnapshot.Content.SECRET.name())));
        LOGGER.info("Player 1 requests player 2 cell - test successful completion");

        // Получение ячейки игрока 2 игроком 2 (палуба)
        LOGGER.info("Player 2 requests player 2 cell - test start");
        mockMvc.perform(get("/game/players/player2/field/cells/1d")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", is(CellSnapshot.Content.DECK.name())));
        LOGGER.info("Player 2 requests player 2 cell - test successful completion");

        // Получение ячейки игрока 1 игроком 2 (секрет)
        LOGGER.info("Player 2 requests player 1 cell - test start");
        mockMvc.perform(get("/game/players/player1/field/cells/1a")
                .contentType(contentType)
                .header("player-name", "player2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", is(CellSnapshot.Content.SECRET.name())));
        LOGGER.info("Player 2 requests player 1 cell - test successful completion");

        // Если ход игрока 1
        if (isPlayer1Turn) {
            // Игрок 1 совершает выстрел в ячейку 1a игрока 2 (попадание)
            Assert.assertEquals(Game.TurnResult.DESTROY_DECK, turn("player1", "player2", "1a"));

            // Получение информации о игре игроком 1 (проверка, что палуба в ячейке 1a уничтожена)
            LOGGER.info("Player 1 requests game info - test start");
            mockMvc.perform(get("/game")
                    .contentType(contentType)
                    .header("player-name", "player1"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.state", is(Game.State.PLAYER1_IS_TURNING.name())))
                    .andExpect(jsonPath("$.player2.field.cells[0][0].content",
                            is(CellSnapshot.Content.DESTROYED_DECK.name())));
            LOGGER.info("Player 1 requests game info - test successful completion");

            // Игрок 1 совершает выстрел в ячейку 1a игрока 2 (ошибка, выстрел в эту ячейку уже производился)
            LOGGER.info("Player 1 attack player 2 cell 1a (error) - test start");
            mockMvc.perform(put("/game/players/player2/field/cells/1a")
                    .contentType(contentType)
                    .header("player-name", "player1"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
            LOGGER.info("Player 1 attack player 2 cell 1a (error)- test successful completion");

            // Игрок 2 совершает выстрел в ячейку 1a игрока 2 (ошибка, не его ход)
            LOGGER.info("Player 2 attack player 1 cell 1a (error) - test start");
            mockMvc.perform(put("/game/players/player1/field/cells/1a")
                    .contentType(contentType)
                    .header("player-name", "player2"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
            LOGGER.info("Player 2 attack player 1 cell 1a (error)- test successful completion");

            // Игрок 1 совершает 2-ой выстрел в ячейку 1b игрока 2 (промах)
            Assert.assertEquals(Game.TurnResult.MISS, turn("player1", "player2", "1b"));

            // Игрок 2 совершает выстрел в ячейку 1a игрока 1 (попадание)
            Assert.assertEquals(Game.TurnResult.DESTROY_DECK, turn("player2", "player1", "1a"));
        } else {
            // Игрок 2 совершает выстрел в ячейку 1a игрока 1 (попадание)
            Assert.assertEquals(Game.TurnResult.DESTROY_DECK, turn("player2", "player1", "1a"));

            // Получение информации о игре игроком 1 (проверка, что палуба в ячейке 1a уничтожена)
            LOGGER.info("Player 2 requests game info - test start");
            mockMvc.perform(get("/game")
                    .contentType(contentType)
                    .header("player-name", "player2"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(contentType))
                    .andExpect(jsonPath("$.state", is(Game.State.PLAYER2_IS_TURNING.name())))
                    .andExpect(jsonPath("$.player1.field.cells[0][0].content",
                            is(CellSnapshot.Content.DESTROYED_DECK.name())));
            LOGGER.info("Player 2 requests game info - test successful completion");

            // Игрок 2 совершает выстрел в ячейку 1a игрока 1 (ошибка, выстрел в эту ячейку уже производился)
            LOGGER.info("Player 2 attack player 1 cell 1a (error) - test start");
            mockMvc.perform(put("/game/players/player1/field/cells/1a")
                    .contentType(contentType)
                    .header("player-name", "player2"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
            LOGGER.info("Player 2 attack player 1 cell 1a (error)- test successful completion");

            // Игрок 1 совершает выстрел в ячейку 1a игрока 2 (ошибка, не его ход)
            LOGGER.info("Player 1 attack player 2 cell 1a (error) - test start");
            mockMvc.perform(put("/game/players/player2/field/cells/1a")
                    .contentType(contentType)
                    .header("player-name", "player1"))
                    .andDo(print())
                    .andExpect(status().isBadRequest());
            LOGGER.info("Player 1 attack player 2 cell 1a (error) - test successful completion");

            // Игрок 2 совершает 2-ой выстрел в ячейку 1b игрока 1 (промах)
            Assert.assertEquals(Game.TurnResult.MISS, turn("player2", "player1", "1b"));

            // Игрок 1 совершает выстрел в ячейку 1a игрока 2 (попадание)
            Assert.assertEquals(Game.TurnResult.DESTROY_DECK, turn("player1", "player2", "1a"));
        }

        // Изменение статуса игрока 2 с "Играет" на "Готов перезапуску игры" игроком 2 (успех)
        LOGGER.info("Player 2 requests to changing player 2 state - test start");
        mockMvc.perform(put("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player2")
                .content("{\"state\":\"" + Player.State.IS_READY_FOR_RESTART.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 2 requests to changing player 2 state - test successful completion");

        // Получение информации о перезапущенной игре игроком 1 (проверка, что игра перезапущена)
        LOGGER.info("Player 1 requests new game info - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player1.field.cells[0][0].content",
                        is(CellSnapshot.Content.DECK.name())))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player2.field.cells[0][0].content",
                        is(CellSnapshot.Content.SECRET.name())));
        LOGGER.info("Player 1 requests new game info - test successful completion");
    }

    /**
     * Тестовой прогон игры с предопределенными ходами игроков
     *
     * @throws Exception в случае возникновения какого-либо исключения
     */
    @Test
    public void game() throws Exception {
        // Получение информации об игре игроком 1
        LOGGER.info("Player 1 requests game info - test start");
        final ResultActions resultActions = mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", isOneOf(
                        Game.State.PLAYER1_IS_TURNING.name(),
                        Game.State.PLAYER2_IS_TURNING.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player1", hasKey("field")))
                .andExpect(jsonPath("$.player1.field.cells[*][*].content", everyItem(isOneOf(
                        CellSnapshot.Content.DECK.name(),
                        CellSnapshot.Content.EMPTINESS.name()))))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_PLAYING.name())))
                .andExpect(jsonPath("$.player2", hasKey("field")))
                .andExpect(jsonPath("$.player2.field.cells[*][*].content", everyItem(
                        is(CellSnapshot.Content.SECRET.name()))));
        LOGGER.info("Player 1 requests game info - test successful completion");

        // Тело ответа
        final String content = resultActions.andReturn().getResponse().getContentAsString();
        // Статус игры
        Game.State state = Game.State.valueOf(JsonPath.read(content, "$.state"));
        // Флаг, указывающий на то, что ожидаемый победитель - игрок 1 (иначе игрок 2)
        final boolean isPlayerMustBeWinner = state == Game.State.PLAYER1_IS_TURNING;

        // Список ходов для игрока 1
        final LinkedList<String> player1Turns = isPlayerMustBeWinner ? getWinnerTurns() : getLooserTurns();
        // Список ходов для игрока 2
        final LinkedList<String> player2Turns = isPlayerMustBeWinner ? getLooserTurns() : getWinnerTurns();

        // Выполняем в цикле ходы игроками
        while (Game.State.in(state, Game.State.PLAYER1_IS_TURNING, Game.State.PLAYER2_IS_TURNING)
                && !player1Turns.isEmpty()
                && !player2Turns.isEmpty()) {
            // Если ходит игрок 1
            if (state == Game.State.PLAYER1_IS_TURNING) {
                // Выполняем выстрел
                final Game.TurnResult result = turn("player1", "player2", player1Turns.pollFirst());
                // Получаем статус игры
                state = getGameState("player1");
                // Проверяем, что при промахе статус игры изменился
                Assert.assertFalse(result == Game.TurnResult.MISS && state == Game.State.PLAYER1_IS_TURNING);
            } else {
                // Выполняем выстрел
                final Game.TurnResult result = turn("player2", "player1", player2Turns.pollFirst());
                // Получаем статус игры
                state = getGameState("player2");
                // Проверяем, что при промахе статус игры изменился
                Assert.assertFalse(result == Game.TurnResult.MISS && state == Game.State.PLAYER2_IS_TURNING);
            }
        }

        // Получение информации об игре игроком 1
        LOGGER.info("Player 1 requests game info - test start");
        mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", "player1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", is(isPlayerMustBeWinner ?
                        Game.State.PLAYER1_HAS_WON.name() :
                        Game.State.PLAYER2_HAS_WON.name())))
                .andExpect(jsonPath("$", hasKey("player1")))
                .andExpect(jsonPath("$.player1.name", is("player1")))
                .andExpect(jsonPath("$.player1.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player1", hasKey("field")))
                .andExpect(jsonPath("$.player1.field.cells[*][*].content", everyItem(
                        not(is(CellSnapshot.Content.SECRET)))))
                .andExpect(jsonPath("$.player1.field.cells[*][?(@.content=='DECK')]",
                        isPlayerMustBeWinner ? not(hasSize(0)) : hasSize(0)))
                .andExpect(jsonPath("$.player1.field.cells[*][?(@.content=='DESTROYED_DECK')]",
                        isPlayerMustBeWinner ? hasSize(8) : hasSize(20)))
                .andExpect(jsonPath("$", hasKey("player2")))
                .andExpect(jsonPath("$.player2.name", is("player2")))
                .andExpect(jsonPath("$.player2.state", is(Player.State.IS_NOT_READY.name())))
                .andExpect(jsonPath("$.player2", hasKey("field")))
                .andExpect(jsonPath("$.player2.field.cells[*][*].content", everyItem(
                        not(is(CellSnapshot.Content.SECRET)))))
                .andExpect(jsonPath("$.player2.field.cells[*][?(@.content=='DECK')]",
                        isPlayerMustBeWinner ? hasSize(0) : not(hasSize(0))))
                .andExpect(jsonPath("$.player2.field.cells[*][?(@.content=='DESTROYED_DECK')]",
                        isPlayerMustBeWinner ? hasSize(20) : hasSize(8)));
        LOGGER.info("Player 1 requests game info - test successful completion");

        // Изменение статуса игрока 1 с "Не готов к игре" на "Готов к игре" игроком 1 (успех)
        LOGGER.info("Player 1 requests to changing player 1 state - test start");
        mockMvc.perform(put("/game/players/player1/state")
                .contentType(contentType)
                .header("player-name", "player1")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 1 requests to changing player 1 state - test successful completion");

        // Изменение статуса игрока 2 с "Не готов к игре" на "Готов к игре" игроком 2 (успех)
        LOGGER.info("Player 2 requests to changing player 2 state - test start");
        mockMvc.perform(put("/game/players/player2/state")
                .contentType(contentType)
                .header("player-name", "player2")
                .content("{\"state\":\"" + Player.State.IS_READY.name() + "\"}"))
                .andDo(print())
                .andExpect(status().isOk());
        LOGGER.info("Player 2 requests to changing player 2 state - test successful completion");

        // Получение статуса игры
        state = getGameState("player1");

        // Проверяем, что запустилась новая игра
        if (!Game.State.in(state, Game.State.PLAYER1_IS_TURNING, Game.State.PLAYER2_IS_TURNING)) {
            // Игра должны была перезапуститься
            Assert.fail();
        }
    }

    /**
     * Получить текущий статус игры
     *
     * @param playerName наименование игрока
     * @return статус игры {@link Game.State}
     * @throws Exception в случае возникновения какого-либо исключения
     */
    private Game.State getGameState(String playerName) throws Exception {
        // Выполняем запрос на получение статуса игры
        LOGGER.info("{} requests game state - test start", playerName);
        final ResultActions resultActions = mockMvc.perform(get("/game")
                .contentType(contentType)
                .header("player-name", playerName))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.state", isOneOf(
                        Game.State.PLAYER1_IS_TURNING.name(),
                        Game.State.PLAYER1_HAS_WON.name(),
                        Game.State.PLAYER2_IS_TURNING.name(),
                        Game.State.PLAYER2_HAS_WON.name())));
        LOGGER.info("{} requests game state - test successful completion", playerName);
        // Тело ответа
        final String content = resultActions.andReturn().getResponse().getContentAsString();
        // Возвращаем статус игры
        return Game.State.valueOf(JsonPath.read(content, "$.state"));
    }

    /**
     * Совершить выстрел
     *
     * @param requestPlayerName наименование выполняюшего запрос игрока
     * @param targetPlayerName  наименование игрока, в ячейку поля которого необходимо совершить выстрел
     * @param cellIndex         индекс ячейки
     * @return результат выстрела {@link Game.TurnResult}
     */
    private Game.TurnResult turn(String requestPlayerName, String targetPlayerName, String cellIndex) throws Exception {
        LOGGER.info("{} attack {} cell - test start", requestPlayerName, targetPlayerName);
        // Выполнить выстрел
        final ResultActions resultActions = mockMvc.perform(
                put("/game/players/" + targetPlayerName + "/field/cells/" + cellIndex)
                        .contentType(contentType)
                        .header("player-name", requestPlayerName))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.result", isOneOf(
                        Game.TurnResult.DESTROY_DECK.name(),
                        Game.TurnResult.DESTROY_SHIP.name(),
                        Game.TurnResult.MISS.name(),
                        Game.TurnResult.WIN.name())))
                .andExpect(jsonPath("$.message", isOneOf(destroyDeck, destroyShip, miss, win)));
        // Тело ответа (ожидается JSON)
        final String jsonBody = resultActions.andReturn().getResponse().getContentAsString();
        // Значение поля "result" (результат выстрела)
        final String result = JsonPath.read(jsonBody, "$.result");
        // Результат выстрела
        final Game.TurnResult turnResult = Game.TurnResult.valueOf(result);
        LOGGER.info("{} attack player {} cell - test successful completion", requestPlayerName, targetPlayerName);
        return turnResult;
    }

    /**
     * @return список ходов для победителя
     */
    private LinkedList<String> getWinnerTurns() {
        // Список ходов победителя
        final LinkedList<String> turns = new LinkedList<>();
        // Попадание в четырехпалубный
        turns.add("1a");
        // Попадание в четырехпалубный
        turns.add("2a");
        // Попадание в четырехпалубный
        turns.add("3a");
        // Уничтожение четырехпалубного
        turns.add("4a");
        // Промах
        turns.add("3d");
        // Попадание в трехпалубный
        turns.add("1d");
        // Попадание в трехпалубный
        turns.add("1e");
        // Уничтожение трехпалубного
        turns.add("1f");
        // Попадание в двухпалубный
        turns.add("1j");
        // Уничтожение двухпалубного
        turns.add("2j");
        // Промах
        turns.add("3h");
        // Попадание в двухпалубный
        turns.add("4f");
        // Уничтожение двухпалубного
        turns.add("4g");
        // Промах
        turns.add("8c");
        // Попадание в двухпалубный
        turns.add("7d");
        // Уничтожение двухпалубного
        turns.add("6d");
        // Уничтожение однопалубного
        turns.add("7g");
        // Промах
        turns.add("10j");
        // Попадание в трехпалубный
        turns.add("10i");
        // Попадание в трехпалубный
        turns.add("10h");
        // Уничтожение трехпалубного
        turns.add("10g");
        // Уничтожение однопалубного
        turns.add("10d");
        // Уничтожение однопалубного
        turns.add("10b");
        // Промах
        turns.add("5j");
        // Уничтожение однопалубного (последнего из всех) и победа
        turns.add("4j");
        // Возвращаем список ходов
        return turns;
    }

    /**
     * @return список ходов для проигравшего
     */
    private LinkedList<String> getLooserTurns() {
        // Список ходов проигравшего
        final LinkedList<String> turns = new LinkedList<>();
        // Попадание в четырехпалубный
        turns.add("1a");
        // Промах
        turns.add("1b");
        // Попадание в четырехпалубный
        turns.add("2a");
        // Попадание в четырехпалубный
        turns.add("3a");
        // Уничтожение четырехпалубного
        turns.add("4a");
        // Промах
        turns.add("6f");
        // Уничтожение однопалубного
        turns.add("7g");
        // Промах
        turns.add("5d");
        // Попадание в трехпалубный
        turns.add("1d");
        // Попадание в трехпалубный
        turns.add("1e");
        // Уничтожение трехпалубного
        turns.add("1f");
        // Промах
        turns.add("6i");
        // Промах
        turns.add("7b");
        // Промах
        turns.add("8i");
        // Возвращаем список ходов
        return turns;
    }
}