package com.test.entity.game;

import com.test.entity.game.player.Player;
import com.test.entity.game.player.PlayerSnapshot;
import com.test.entity.game.player.field.CellAlreadyAttackedException;
import com.test.entity.game.player.field.Field;
import com.test.entity.game.player.field.FieldSnapshot;
import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.cell.CellSnapshot;
import com.test.entity.game.player.field.converter.ColumnIndexConverter;
import com.test.entity.game.player.field.placement.PlacementExecutor;
import com.test.entity.game.player.field.printer.FieldPrinter;
import com.test.entity.game.player.field.ship.Ship;
import com.test.entity.game.player.field.ship.deck.Deck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Игра
 */
public class Game implements ApplicationContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(Game.class); // Логгер

    /**
     * Возможные статусы игры
     */
    public enum State {
        PLAYERS_IS_NOT_READY, // Ожидание готовности игроков
        PLAYER1_IS_TURNING, // Ход игрока 1
        PLAYER2_IS_TURNING, // Ход игрока 2
        PLAYER1_HAS_WON, // Игрок 1 победил
        PLAYER2_HAS_WON; // Игрок 2 победил

        /**
         * Проверить, что статус содержится в массиве статусов
         *
         * @param state  проверяемый статус
         * @param states массив статусов в котором будет произведен поиск
         * @return {@code true}, если статус содержится в массиве
         */
        public static boolean in(State state, State... states) {
            if (states != null) {
                for (State s : states) {
                    if (s == state) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Возможные результаты хода игрока
     */
    public enum TurnResult {
        DESTROY_DECK, // Уничтожена палуба
        DESTROY_SHIP, // Уничтожен корабль
        MISS, // Промах
        WIN // Победа
    }

    private ApplicationContext applicationContext; // Фабрика бинов Spring

    private final Player player1; // Игрок 1
    private final Player player2; // Игрок 2
    private final PlacementExecutor placementExecutor; // Класс, выполняющий размещение кораблей на игровом поле
    private final ColumnIndexConverter columnIndexConverter; // Класс, выполняющий преобразование индексов столбцов
    private final FieldPrinter printer; // Принтер для печати полей игроков
    private State state; // Статус игры

    /**
     * @param player1              игрок 1
     * @param player2              игрок 2
     * @param placementExecutor    класс, выполняющий размещение кораблей на игровом поле
     * @param columnIndexConverter класс, выполняющий преобразование индексов столбцов
     * @param printer              принтер для печати полей игроков
     * @throws IllegalArgumentException если один из переданных аргументов = {@code null}
     */
    public Game(
            Player player1,
            Player player2,
            PlacementExecutor placementExecutor,
            ColumnIndexConverter columnIndexConverter,
            FieldPrinter printer
    ) {
        // Проверяем входные данные на null
        Assert.notNull(player1);
        Assert.notNull(player2);
        Assert.notNull(placementExecutor);
        Assert.notNull(columnIndexConverter);
        Assert.notNull(printer);
        // Устанавливаем входные данные
        this.player1 = player1;
        this.player2 = player2;
        this.placementExecutor = placementExecutor;
        this.columnIndexConverter = columnIndexConverter;
        this.printer = printer;
        // Устанваливам игрокам статус "Не готов к игре"
        player1.setState(Player.State.IS_NOT_READY);
        player2.setState(Player.State.IS_NOT_READY);
        // Переводим игру в состояние "Ожидание готовности игроков"
        state = State.PLAYERS_IS_NOT_READY;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * Получить снимок информации об игре
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @return снимок информации об игре {@link GameSnapshot}
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized GameSnapshot getSnapshot(String requestPlayerName) {
        // Проверяем, что запрос выполнил игрок 1 или игрок 2
        checkPlayerName(requestPlayerName);
        // Создаем снимок с информацией об игре
        final GameSnapshot snapshot = applicationContext.getBean("gameSnapshot", GameSnapshot.class);
        // Задаем статус
        snapshot.setState(state);
        // Задаем снимок информации об игроке 1
        snapshot.setPlayer1(getPlayerSnapshot(requestPlayerName, player1.getName()));
        // Задаем снимок информации об игроке 2
        snapshot.setPlayer2(getPlayerSnapshot(requestPlayerName, player2.getName()));
        // Возвращаем снимок
        return snapshot;
    }

    /**
     * Получить снимок информации об игроке
     * Смотрите также {@link #createPlayerSnapshot(Player, boolean)}
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, информацию о котором необходимо получить
     * @return снимок информации об игроке {@link PlayerSnapshot}
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized PlayerSnapshot getPlayerSnapshot(String requestPlayerName, String targetPlayerName) {
        // Флаг, указывающий на то, что запрос выполнил игрок 1
        final boolean isPlayer1Request = Objects.equals(requestPlayerName, player1.getName());
        // Флаг, указывающий на то, что запрос выполнил игрок 2
        final boolean isPlayer2Request = Objects.equals(requestPlayerName, player2.getName());

        // Если запрос выполнил игрок 1 или игрок 2
        if (isPlayer1Request || isPlayer2Request) {
            // Если запрашивается информация об игроке 1
            if (Objects.equals(targetPlayerName, player1.getName())) {
                // Возвращаем актуальную информацию об игре
                return createPlayerSnapshot(player1, !gameIsOver() && isPlayer2Request);
            }
            // Если запрашивается информация об игроке 2
            if (Objects.equals(targetPlayerName, player2.getName())) {
                // Возвращаем актуальную информацию об игре 2
                return createPlayerSnapshot(player2, !gameIsOver() && isPlayer1Request);
            }
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(targetPlayerName);
        }

        // Выбрасываем исключение, если не удалось определить игрока
        throw new UnknownPlayerException(requestPlayerName);
    }

    /**
     * Получить снимок игровом поля
     * Смотрите также {@link #createFieldSnapshot(Field, boolean)}
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, поле которого необходимо получить
     * @return снимок игрового поля {@link FieldSnapshot}
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized FieldSnapshot getFieldSnapshot(String requestPlayerName, String targetPlayerName) {
        // Флаг, указывающий на то, что запрос выполнил игрок 1
        final boolean isPlayer1Request = Objects.equals(requestPlayerName, player1.getName());
        // Флаг, указывающий на то, что запрос выполнил игрок 2
        final boolean isPlayer2Request = Objects.equals(requestPlayerName, player2.getName());

        // Если запрос выполнил игрок 1 или игрок 2
        if (isPlayer1Request || isPlayer2Request) {
            // Если запрашивается статус игрока 1
            if (Objects.equals(targetPlayerName, player1.getName())) {
                // Возвращаем поле игрока 1 или null, если оно не задано
                return player1.getField() != null ? createFieldSnapshot(player1.getField(), isPlayer2Request) : null;
            }
            // Если запрашивается статус игрока 2
            if (Objects.equals(targetPlayerName, player2.getName())) {
                // Возвращаем поле игрока 2 или null, если оно не задано
                return player2.getField() != null ? createFieldSnapshot(player2.getField(), isPlayer1Request) : null;
            }
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(targetPlayerName);
        }

        // Выбрасываем исключение, если не удалось определить игрока
        throw new UnknownPlayerException(requestPlayerName);
    }

    /**
     * Получить снимок ячейки игрового поля
     * Смотрите также {@link #createCellSnapshot(Cell, boolean)}
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, поле которого необходимо получить
     * @param cellIndex         индекс ячейки
     * @return снимок ячейки игрового поля {@link CellSnapshot}
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized CellSnapshot getCellSnapshot(
            String requestPlayerName,
            String targetPlayerName,
            String cellIndex
    ) {
        // Флаг, указывающий на то, что запрос выполнил игрок 1
        final boolean isPlayer1Request = Objects.equals(requestPlayerName, player1.getName());
        // Флаг, указывающий на то, что запрос выполнил игрок 2
        final boolean isPlayer2Request = Objects.equals(requestPlayerName, player2.getName());

        // Если запрос выполнил игрок 1 или игрок 2
        if (isPlayer1Request || isPlayer2Request) {
            // Номер строки
            final int row = getRowFromCellIndex(cellIndex);
            // Номер столбца
            final int column = getColumnFromCellIndex(cellIndex);

            // Если запрашивается статус игрока 1
            if (Objects.equals(targetPlayerName, player1.getName())) {
                // Возвращаем поле игрока 1 или null, если оно не задано
                return player1.getField() != null ?
                        createCellSnapshot(player1.getField().getCell(row, column), isPlayer2Request) :
                        null;
            }
            // Если запрашивается статус игрока 2
            if (Objects.equals(targetPlayerName, player2.getName())) {
                // Возвращаем поле игрока 2 или null, если оно не задано
                return player2.getField() != null ?
                        createCellSnapshot(player2.getField().getCell(row, column), isPlayer1Request) :
                        null;
            }
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(targetPlayerName);
        }

        // Выбрасываем исключение, если не удалось определить игрока
        throw new UnknownPlayerException(requestPlayerName);
    }

    /**
     * Получить статус игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, статус которого необходимо получить
     * @return статус игрока {@link Player.State}
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized Player.State getPlayerState(String requestPlayerName, String targetPlayerName) {
        // Флаг, указывающий на то, что запрос выполнил игрок 1
        final boolean isPlayer1Request = Objects.equals(requestPlayerName, player1.getName());
        // Флаг, указывающий на то, что запрос выполнил игрок 2
        final boolean isPlayer2Request = Objects.equals(requestPlayerName, player2.getName());

        // Если запрос выполнил игрок 1 или игрок 2
        if (isPlayer1Request || isPlayer2Request) {
            // Если запрашивается статус игрока 1
            if (Objects.equals(targetPlayerName, player1.getName())) {
                // Возвращаем статус игрока 1
                return player1.getState();
            }
            // Если запрашивается статус игрока 2
            if (Objects.equals(targetPlayerName, player2.getName())) {
                // Возвращаем статус игрока 2
                return player2.getState();
            }
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(targetPlayerName);
        }

        // Выбрасываем исключение, если не удалось определить игрока
        throw new UnknownPlayerException(requestPlayerName);
    }

    /**
     * Изменить статус игрока
     * Если оба игрока заявляют о готовности, то стартует новая игра (смотрите {@link #restart()})
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, статус которого необходимо изменить
     * @param newPlayerState    новый статус игрока
     * @throws IllegalActionException если изменение на указанный статус запрещено при текущем состоянии игры
     * @throws UnknownPlayerException если не удалось определить игрока
     */
    public synchronized void changePlayerState(
            String requestPlayerName,
            String targetPlayerName,
            Player.State newPlayerState
    ) {
        // Проверяем, что запрос выполнил игрок 1 или игрок 2
        checkPlayerName(requestPlayerName);

        // Проверяем, что игрок меняет свой статус
        if (!Objects.equals(requestPlayerName, targetPlayerName)) {
            // Выбрасываем исключение
            throw new IllegalActionException();
        }

        // Проверяем новый статус
        if (!checkNewPlayerState(newPlayerState)) {
            throw new IllegalActionException();
        }

        // Определяем игрока
        if (Objects.equals(targetPlayerName, player1.getName())) {
            // Задаем готовность для игрока 1
            player1.setState(newPlayerState);
            // Логгируем изменение статуса
            LOGGER.info("Player {} has changed state to: {}", player1.getName(), player1.getState().name());
        } else if (Objects.equals(targetPlayerName, player2.getName())) {
            // Задаем готовность для игрока 2
            player2.setState(newPlayerState);
            // Логгируем изменение статуса
            LOGGER.info("Player {} has changed state to: {}", player2.getName(), player2.getState().name());
        } else {
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(targetPlayerName);
        }

        // Если оба игрока готовы к игре
        if (player1.getState() == Player.State.IS_READY && player2.getState() == Player.State.IS_READY) {
            // Стартуем новую игру
            restart();
            return;
        }

        // Если оба игрока готовы к перезапуску игры
        if (player1.getState() == Player.State.IS_READY_FOR_RESTART
                && player2.getState() == Player.State.IS_READY_FOR_RESTART) {
            // Перезапускаем игру
            restart();
        }
    }

    /**
     * Выполнить ход
     * Смотрите также {@link #turn(String, boolean)}
     *
     * @param requestPlayerName наименование выполневшего запрос игрока
     * @param targetPlayerName  наименование игрока, выстрел в поле которого необходимо совершить
     * @param cellIndex         индекс ячейки
     * @return результат хода {@link TurnResult}
     * @throws IllegalActionException при попытке выполнить ход в тот момент, когда игра еще не запущена
     * @throws UnknownPlayerException если не удалось определить игрока
     * @throws MoveOutOfTurnException при попытке выполнить ход вне очереди
     */
    public synchronized TurnResult turn(String requestPlayerName, String targetPlayerName, String cellIndex) {
        // Проверяем, что запрос выполнил игрок 1 или игрок 2
        checkPlayerName(requestPlayerName);
        // Проверяем, что на текущем статусе игры ходить вообще можно
        if (state != State.PLAYER1_IS_TURNING && state != State.PLAYER2_IS_TURNING) {
            // Выбрасываем исключение
            throw new IllegalActionException();
        }
        // Проверяем, что игрок выполняет выстрел в поле соперника
        if (Objects.equals(requestPlayerName, targetPlayerName)) {
            // Выбрасываем исключение
            throw new IllegalActionException();
        }
        // Определяем соперника
        if (Objects.equals(targetPlayerName, player1.getName())) {
            // Выстрел в поле игрока 1
            return turn(cellIndex, false);
        }
        if (Objects.equals(targetPlayerName, player2.getName())) {
            // Выстрел в поле игрока 2
            return turn(cellIndex, true);
        }
        // Выбрасываем исключение, если не удалось определить игрока
        throw new UnknownPlayerException(requestPlayerName);
    }

    /**
     * Создать снимок информации об игроке
     *
     * @param player    игрок {@link Player}
     * @param hideShips флаг, указывающий на то, что необходимо скрыть дееспособные палубы игрока
     * @return снимок информации об игроке {@link PlayerSnapshot}
     */
    private PlayerSnapshot createPlayerSnapshot(Player player, boolean hideShips) {
        // Создаем снимок с информацией об игроке
        final PlayerSnapshot snapshot = applicationContext.getBean("playerSnapshot", PlayerSnapshot.class);
        // Задаем статус
        snapshot.setState(player.getState());
        // Задаем наименование
        snapshot.setName(player.getName());
        // Если игровое поле установлено
        if (player.getField() != null) {
            // Создаем и устанавливаем снимок с информацией об игровом поле
            snapshot.setField(createFieldSnapshot(player.getField(), hideShips));
        }
        // Возвращаем созданный снимок
        return snapshot;
    }

    /**
     * Создать снимок игрового поля
     *
     * @param field     игровое поле {@link Field}
     * @param hideShips флаг, указывающий на то, что необходимо скрыть дееспособные палубы игрока
     * @return снимок информации об игровом поле {@link FieldSnapshot}
     */
    private FieldSnapshot createFieldSnapshot(Field field, boolean hideShips) {
        // Количество строк
        final int rows = field.getCells().length;
        // Количество столбцов
        final int columns = field.getCells()[0].length;

        // Массив ячеек
        final CellSnapshot[][] cellSnapshots = new CellSnapshot[rows][columns];
        // Заполняем массив ячеек
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                // Оригинальная ячейка с заданными координатами
                final Cell cell = field.getCell(row, column);
                // Создаем снимок ячейки и помещаем его в массив
                cellSnapshots[row][column] = createCellSnapshot(cell, hideShips);
            }
        }

        // Создаем снимок с информацией об игровом поле
        final FieldSnapshot snapshot = applicationContext.getBean("fieldSnapshot", FieldSnapshot.class);
        // Устанавливаем массив ячеек
        snapshot.setCells(cellSnapshots);
        // Возвращаем созданный снимок
        return snapshot;
    }

    /**
     * Создать снимок ячейки игрового поля
     *
     * @param cell      ячейка игрового поля {@link Cell}
     * @param hideShips флаг, указывающий на то, что необходимо скрыть дееспособные палубы игрока
     * @return снимок информации об игровом поле {@link CellSnapshot}
     */
    private CellSnapshot createCellSnapshot(Cell cell, boolean hideShips) {
        // Создаем снимок ячейки игрового поля
        CellSnapshot snapshot = applicationContext.getBean("cellSnapshot", CellSnapshot.class);
        // Задаем номер строки
        snapshot.setRow(String.valueOf(cell.getRow() + 1));
        // Задаем псевдоним столбца
        snapshot.setColumn(columnIndexConverter.getAlias(cell.getColumn()));
        // Указываем тип содержимого
        // Если ячейка содержит палубу корабля
        if (cell.hasDeck()) {
            // Если в ячейку производился выстрел
            if (cell.isAttacked()) {
                // "Уничтоженная палуба"
                snapshot.setContent(CellSnapshot.Content.DESTROYED_DECK);
            } else {
                // Если корабли необходимо скрыть, то "Содержание не известно", иначе "Дееспособная палуба корабля"
                snapshot.setContent(hideShips ? CellSnapshot.Content.SECRET : CellSnapshot.Content.DECK);
            }
        } else {
            // Если в ячейку производился выстрел
            if (cell.isAttacked()) {
                // "Промах"
                snapshot.setContent(CellSnapshot.Content.MISS);
            } else {
                // Если корабли необходимо скрыть, то "Содержание не известно", иначе "Пустота"
                snapshot.setContent(hideShips ? CellSnapshot.Content.SECRET : CellSnapshot.Content.EMPTINESS);
            }
        }
        // Возвращаем созданный снимок
        return snapshot;
    }

    /**
     * Проверка наименования игрока
     *
     * @param playerName наименование игрока
     * @throws UnknownPlayerException если переданное наименования не равно ни наименованию игрока 1, ни игрока 2
     */
    private void checkPlayerName(String playerName) {
        // Проверяем, что наименование игрока = наименованию игрока 1 или игрока 2
        if (!Objects.equals(playerName, player1.getName()) && !Objects.equals(playerName, player2.getName())) {
            // Выбрасываем исключение, если не удалось определить игрока
            throw new UnknownPlayerException(playerName);
        }
    }

    /**
     * Проверка, что новый статус игрока доступен на текущем статусе игры
     *
     * @param newPlayerState новый статуса игрока {@link Player.State}
     * @return {@code true}, если проверка пройдена
     */
    private boolean checkNewPlayerState(Player.State newPlayerState) {
        // Если игра не запущена
        if (state == State.PLAYERS_IS_NOT_READY) {
            // Если новый статус игрока = "Играет" или "Играет, но готов перезапуску игры"
            if (Player.State.in(newPlayerState, Player.State.IS_PLAYING, Player.State.IS_READY_FOR_RESTART)) {
                // Проверка не пройдена
                return false;
            }
            // Проверка пройдена
            return true;
        }
        // Если игра запущена
        if (State.in(state, State.PLAYER1_IS_TURNING, State.PLAYER2_IS_TURNING)) {
            // Если новый статус игрока = "Не готов к игре" или "Готов к игре"
            if (Player.State.in(newPlayerState, Player.State.IS_NOT_READY, Player.State.IS_READY)) {
                // Проверка не пройдена
                return false;
            }
            // Проверка пройдена
            return true;
        }
        // Если игра окончена
        if (State.in(state, State.PLAYER1_HAS_WON, State.PLAYER2_HAS_WON)) {
            // Если новый статус игрока = "Не готов к игре" или "Готов к игре"
            if (Player.State.in(newPlayerState, Player.State.IS_PLAYING, Player.State.IS_READY_FOR_RESTART)) {
                // Проверка не пройдена
                return false;
            }
            // Проверка пройдена
            return true;
        }
        // Проверка не пройдена
        return false;
    }

    /**
     * Проверить индекс ячейки на корректность
     *
     * @param cellIndex индекс ячейки
     * @throws BadCellIndexException если переданный индекс некорректен
     */
    private void checkCellIndex(String cellIndex) {
        // Проверяем индекс ячейки на корректность
        if (StringUtils.isEmpty(cellIndex) || !(cellIndex.length() == 2 || cellIndex.length() == 3)) {
            // Выбрасываем исключение
            throw new BadCellIndexException(cellIndex);
        }
    }

    /**
     * Извлечь номер строки из индекса ячейки
     *
     * @param cellIndex индекс ячейки
     * @return номер строки
     * @throws BadCellIndexException если переданный индекс некорректен
     */
    private int getRowFromCellIndex(String cellIndex) {
        // Проверяем индекс ячейки на корректность
        checkCellIndex(cellIndex);
        // Определяем и возвращаем номер строки
        return Integer.parseInt(cellIndex.substring(0, cellIndex.length() == 2 ? 1 : 2)) - 1;
    }

    /**
     * Извлечь номер столбца из индекса ячейки
     *
     * @param cellIndex индекс ячейки
     * @return номер столбца
     * @throws BadCellIndexException если переданный индекс некорректен
     */
    private int getColumnFromCellIndex(String cellIndex) {
        // Проверяем индекс ячейки на корректность
        checkCellIndex(cellIndex);
        // Определяем и возвращаем номер столбца
        return columnIndexConverter.getNumber(cellIndex.substring(cellIndex.length() == 2 ? 1 : 2));
    }

    /**
     * Выполнить ход
     *
     * @param cellIndex индекс ячейки
     * @param isPlayer1 флаг, указывающий на то, что это ход игрока 1
     * @throws MoveOutOfTurnException       при попытке выполнить ход вне очереди
     * @throws CellAlreadyAttackedException если в указанную ячейку выстрел уже производился
     */
    private TurnResult turn(String cellIndex, boolean isPlayer1) {
        // Проверяем, что игрок ходит в свою очередь
        if ((isPlayer1 && state != State.PLAYER1_IS_TURNING)
                || (!isPlayer1 && state != State.PLAYER2_IS_TURNING)) {
            // Выбрасываем исключение, если ход не в свою очередь
            throw new MoveOutOfTurnException(isPlayer1 ? player1.getName() : player2.getName());
        }

        // Номер строки
        final int row = getRowFromCellIndex(cellIndex);
        // Номер столбца
        final int column = getColumnFromCellIndex(cellIndex);

        // Результат хода
        final TurnResult result;
        // Подбитая палуба
        final Deck deck;
        // Флаг, указывающий на то, что ход был победным
        final boolean isWinner;

        if (isPlayer1) {
            // Совершаем выстрел
            deck = player2.getField().getCell(row, column).attack();
            // Если было попадание
            if (deck != null) {
                // Определяем, является ли текущий ход победным
                isWinner = Stream.of(player2.getShips()).filter(ship -> !ship.isDestroyed()).count() == 0;
                // Если ход является победным
                if (isWinner) {
                    // Игра переходит в состояние "Игрок 1 победил"
                    state = State.PLAYER1_HAS_WON;
                }
            } else {
                // Ход не является победным
                isWinner = false;
                // Игра переходит в состояние "Ход игрока 2"
                state = State.PLAYER2_IS_TURNING;
            }
        } else {
            // Совершаем выстрел
            deck = player1.getField().getCell(row, column).attack();
            // Если было попадание
            if (deck != null) {
                // Определяем, является ли текущий ход победным
                isWinner = Stream.of(player1.getShips()).filter(ship -> !ship.isDestroyed()).count() == 0;
                // Если ход является победным
                if (isWinner) {
                    // Игра переходит в состояние "Игрок 2 победил"
                    state = State.PLAYER2_HAS_WON;
                }
            } else {
                // Ход не является победным
                isWinner = false;
                // Игра переходит в состояние "Ход игрока 1"
                state = State.PLAYER1_IS_TURNING;
            }
        }

        // Если ход является победным
        if (isWinner) {
            // Устанваливам обоим игрокам статус "Не готов к игре"
            player1.setState(Player.State.IS_NOT_READY);
            player2.setState(Player.State.IS_NOT_READY);
            // Результат хода - победа
            result = TurnResult.WIN;
        } else if (deck != null) {
            // Определяем результаты попадания во вражеский корабль (резултат хода - уничтожен/подбит)
            result = deck.getShip().isDestroyed() ? TurnResult.DESTROY_SHIP : TurnResult.DESTROY_DECK;
        } else {
            // Резултат хода - промах
            result = TurnResult.MISS;
        }

        // Логгируем результат хода
        LOGGER.info(
                "Player {} has turn! Result of turn: {}",
                isPlayer1 ? player1.getName() : player2.getName(),
                result.name()
        );

        // Печатаем состоние поля игрока 1
        LOGGER.info("Player {} field:", player1.getName());
        printer.printField(player1.getField());

        // Печатаем состоние поля игрока 2
        LOGGER.info("Player {} field:", player2.getName());
        printer.printField(player2.getField());

        // Возвращаем результат хода
        return result;
    }

    /**
     * @return {@code true}, если игра окончена
     */
    private boolean gameIsOver() {
        return State.in(state, State.PLAYER1_HAS_WON, State.PLAYER2_HAS_WON);
    }

    /**
     * Перезапуск игры
     * Кто из игроков ходит первым - определяется рандомным образом
     */
    private void restart() {
        // Создаем игрокам поля
        player1.setField(createField());
        player2.setField(createField());

        // Создаем игрокам корабли
        player1.setShips(createShips());
        player2.setShips(createShips());

        // Размещаем игрокам корабли
        placementExecutor.execute(player1.getField().getCells(), player1.getShips());
        placementExecutor.execute(player2.getField().getCells(), player2.getShips());

        // Устанваливам игрокам статус "Играет"
        player1.setState(Player.State.IS_PLAYING);
        player2.setState(Player.State.IS_PLAYING);

        // Определяем рандомным образом кто из игроков ходит первым
        state = new Random().nextBoolean() ? State.PLAYER1_IS_TURNING : State.PLAYER2_IS_TURNING;

        // Логируем информацию о запуске новой игры
        LOGGER.info("New game has started!");

        // Печатаем состоние поля игрока 1
        LOGGER.info("Player {} field:", player1.getName());
        printer.printField(player1.getField());

        // Печатаем состоние поля игрока 2
        LOGGER.info("Player {} field:", player2.getName());
        printer.printField(player2.getField());
    }

    /**
     * Создать игровое поле для игрока
     *
     * @return игровое поле {@link Field}
     */
    private Field createField() {
        return applicationContext.getBean("field", Field.class);
    }

    /**
     * Создать корабли для игрока
     *
     * @return корабли {@link Ship}
     */
    private Ship[] createShips() {
        // Создаем массив кораблей
        final Ship[] ships = new Ship[10];
        // Заполняем массив кораблями
        int i = 0;
        // 1 четырехпалубный
        ships[i] = applicationContext.getBean("fourDecksShip", Ship.class);
        // 2 трехпалубных
        ships[++i] = applicationContext.getBean("treeDecksShip", Ship.class);
        ships[++i] = applicationContext.getBean("treeDecksShip", Ship.class);
        // 3 двухпалубных
        ships[++i] = applicationContext.getBean("twoDecksShip", Ship.class);
        ships[++i] = applicationContext.getBean("twoDecksShip", Ship.class);
        ships[++i] = applicationContext.getBean("twoDecksShip", Ship.class);
        // 4 однопалубных
        ships[++i] = applicationContext.getBean("oneDeckShip", Ship.class);
        ships[++i] = applicationContext.getBean("oneDeckShip", Ship.class);
        ships[++i] = applicationContext.getBean("oneDeckShip", Ship.class);
        ships[++i] = applicationContext.getBean("oneDeckShip", Ship.class);
        // Возвращаем массив кораблей
        return ships;
    }
}
