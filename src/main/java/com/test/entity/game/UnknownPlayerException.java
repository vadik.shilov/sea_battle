package com.test.entity.game;

import com.test.SeaBattleException;

/**
 * Неизвестный игрок
 */
public class UnknownPlayerException extends SeaBattleException {
    private final String name; // Наименование игрока

    UnknownPlayerException(String name) {
        this.name = name;
    }

    /**
     * @return наименование игрока
     */
    public String getName() {
        return name;
    }
}
