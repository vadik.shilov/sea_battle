package com.test.entity.game.player.field.ship.deck;

import com.test.entity.game.player.field.ship.Ship;
import org.springframework.util.Assert;

/**
 * Палуба корабля
 */
public class Deck {
    private Ship ship; // Корабль, которому пренадлежит данная палуба
    private boolean destroyed; // Флаг, указывающий на то, что палуба уничтожена

    /**
     * @return карабль, которому пренадлежит данная палуба {@link Ship}
     */
    public Ship getShip() {
        return ship;
    }

    /**
     * Указать карабль, которому пренадлежит данная палуба
     *
     * @param ship корабль {@link Ship}
     * @throws IllegalArgumentException если в качестве корабля передан {@code null}
     */
    public void setShip(Ship ship) {
        // Проверяем корабль на null
        Assert.notNull(ship);
        // Задаем корабль
        this.ship = ship;
    }

    /**
     * @return {@code true}, если палуба уничтожена
     */
    public boolean isDestroyed() {
        return destroyed;
    }

    /**
     * Уничтожить палубу
     */
    public void destroy() {
        destroyed = true;
    }
}
