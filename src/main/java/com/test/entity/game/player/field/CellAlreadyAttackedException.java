package com.test.entity.game.player.field;

import com.test.SeaBattleException;

/**
 * В указанную ячейку выстрел уже производился
 */
public class CellAlreadyAttackedException extends SeaBattleException {
    private final int row; // Номер строки
    private final int column; // Номер столбца

    /**
     * @param row    номер строки
     * @param column номер столбца
     */
    public CellAlreadyAttackedException(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * @return номер строки
     */
    public int getRow() {
        return row;
    }

    /**
     * @return номер столбца
     */
    public int getColumn() {
        return column;
    }
}
