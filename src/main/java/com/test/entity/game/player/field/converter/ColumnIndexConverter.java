package com.test.entity.game.player.field.converter;

import com.test.entity.game.player.field.BadCoordinateException;

import java.util.LinkedList;
import java.util.List;

/**
 * Преобразователь индексов столбцов
 * номера столбца -> псевдоним
 * псевдоним столбца -> номер
 */
public class ColumnIndexConverter {
    private final List<String> aliases = new LinkedList<>(); // Упорядоченный список псевдонимов

    {   // Инициализируем список
        aliases.add("a");
        aliases.add("b");
        aliases.add("c");
        aliases.add("d");
        aliases.add("e");
        aliases.add("f");
        aliases.add("g");
        aliases.add("h");
        aliases.add("i");
        aliases.add("j");
    }

    /**
     * Получить номер столбца по его псевдониму
     *
     * @param alias псевдоним
     * @return номер
     * @throws BadColumnAliasException если передан некорректный псевдоним столбца
     */
    public int getNumber(String alias) {
        // Определяем номер псевданима в списке
        final int index = aliases.indexOf(alias);
        // Если такой псевдоним не содержится
        if (index == -1) {
            // Выбрасываем исключение
            throw new BadColumnAliasException(alias);
        }
        // Возвращаем номер
        return index;
    }

    /**
     * Получить псевдоним столбца по его номеру
     *
     * @param number номер
     * @return псевдоним
     * @throws BadCoordinateException если передан некорректный номер столбца
     */
    public String getAlias(int number) {
        // Если номер не корректен
        if (number < 0 || number >= aliases.size()) {
            // Выбрасываем исключение
            throw new BadCoordinateException(number, false);
        }
        // Возвращаем псевдоним столбца по номеру
        return aliases.get(number);
    }
}
