package com.test.entity.game.player.field.placement.algorithm.random;

import java.util.function.IntPredicate;

/**
 * Генератор данных для расположения кораблей на игровом поле
 */
public class ShipsPositionsGenerator {
    private final int rows; // Количество строк у игрового поля
    private final int columns; // Количество столбцов у игрового поля
    private final PositionIterator[] ships;
    private final boolean[][] cells;

    /**
     * @param rows    количество строк у игрового поля
     * @param columns количество столбцов у игрового поля
     */
    public ShipsPositionsGenerator(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        int i = 0;
        ships = new PositionIterator[10];
        ships[i] = new PositionIterator(4, rows, columns);
        ships[++i] = new PositionIterator(3, rows, columns);
        ships[++i] = new PositionIterator(3, rows, columns);
        ships[++i] = new PositionIterator(2, rows, columns);
        ships[++i] = new PositionIterator(2, rows, columns);
        ships[++i] = new PositionIterator(2, rows, columns);
        ships[++i] = new PositionIterator(1, rows, columns);
        ships[++i] = new PositionIterator(1, rows, columns);
        ships[++i] = new PositionIterator(1, rows, columns);
        ships[++i] = new PositionIterator(1, rows, columns);
        cells = new boolean[rows][columns];
    }

    /**
     * Сгенерировать позиции кораблей
     *
     * @return массив позиций для кораблей
     */
    public PositionIterator[] generatePositions() {
        int shipIndex = 0;
        while (shipIndex < ships.length) {
            final PositionIterator shipPosition = ships[shipIndex];
            if (checkPosition(shipPosition)) {
                updateCells(shipPosition, true);
                shipIndex++;
            } else if (shipPosition.hasNext()) {
                shipPosition.next();
            } else if (shipIndex > 0 && shipIndex < ships.length - 1) {
                shipPosition.reset();
                shipIndex--;
                final PositionIterator previousShipPosition = ships[shipIndex];
                updateCells(previousShipPosition, false);
                if (previousShipPosition.hasNext()) {
                    shipPosition.next();
                } else {
                    throw new IllegalStateException();
                }
            } else {
                throw new IllegalStateException();
            }
        }
        return ships;
    }

    /**
     * @param position позиция корабля {@link PositionIterator}
     * @param reserve  {@code true}, если необходимо зарезервировать ячейки, {@code false} - освободить
     */
    private void updateCells(PositionIterator position, boolean reserve) {
        // Если корабль расположен горизонтально
        if (position.getAngle() == 0) {
            for (int i = position.getColumn(); i < position.getColumn() + position.getSize(); i++) {
                // Обновляем ячейку (true - занята, false - свободна)
                cells[position.getRow()][i] = reserve;
            }
        } else {
            for (int i = position.getRow(); i < position.getRow() + position.getSize(); i++) {
                // Обновляем ячейку (true - занята, false - свободна)
                cells[i][position.getColumn()] = reserve;
            }
        }
    }

    /**
     * @param position позиция корабля {@link PositionIterator}
     * @return {@code true}, если проверка пройдена
     */
    private boolean checkPosition(PositionIterator position) {
        // Флаг, указывающий на то, что корабль расположен горизонтально
        final boolean isHorizontal = position.getAngle() == 0;
        // Начальный индекс
        final int startIndex = isHorizontal ? position.getColumn() : position.getRow();
        // Конечный индекс
        final int endIndex = startIndex + position.getSize() - 1;
        // Проверяем, что корабль не выходит за пределы поля
        if (endIndex >= (isHorizontal ? columns : rows)) {
            // Проверка не пройдена
            return false;
        }
        // Функция, необходимая для проверки того,
        // что данная позиция не пересекается с позициями других кораблей
        final IntPredicate collisionChecker;
        // Функция, необходимая для проверки того,
        // что на границе корабля в одну ячейку нет других кораблей
        final IntPredicate borderChecker;
        // Если корабль расположен горизонтально
        if (isHorizontal) {
            collisionChecker = i -> cells[position.getRow()][i];
            // Флаг, указывающий на то, что необходимо проверять границу сверху от корабля
            final boolean checkTop = position.getRow() - 1 >= 0;
            // Флаг, указывающий на то, что необходимо проверять границу снизу от корабля
            final boolean checkBottom = position.getRow() + 1 < rows;
            if (checkTop && checkBottom) {
                // Проверяем и сверху и снизу
                borderChecker = i -> cells[position.getRow() - 1][i] || cells[position.getRow() + 1][i];
            } else if (checkTop) {
                // Проверяем только сверху
                borderChecker = i -> cells[position.getRow() - 1][i];
            } else {
                // Проверяем только снизу
                borderChecker = i -> cells[position.getRow() + 1][i];
            }
        } else {
            collisionChecker = i -> cells[i][position.getColumn()];
            // Флаг, указывающий на то, что необходимо проверять границу слева от корабля
            final boolean checkLeft = position.getColumn() - 1 >= 0;
            // Флаг, указывающий на то, что необходимо проверять границу справа от корабля
            final boolean checkRight = position.getColumn() + 1 < columns;
            if (checkLeft && checkRight) {
                // Проверяем и слева и справа
                borderChecker = i -> cells[i][position.getColumn() - 1] || cells[i][position.getColumn() + 1];
            } else if (checkLeft) {
                // Проверяем только слева
                borderChecker = i -> cells[i][position.getColumn() - 1];
            } else {
                // Проверяем только справа
                borderChecker = i -> cells[i][position.getColumn() + 1];
            }
        }
        // Проверяем, что указанная позиция не пересекается с позициями других кораблей
        for (int i = startIndex; i <= endIndex; i++) {
            // Если в указанной ячейке располагается палуба другого корабля
            if (collisionChecker.test(i)) {
                // Проверка не пройдена
                return false;
            }
            // Если на границе указанной ячейки располагается палуба другого корабля
            if (borderChecker.test(i)) {
                // Проверка не пройдена
                return false;
            }
        }
        // Номер индекса перед начальным
        final int beforeStartIndex = startIndex - 1;
        // Если на границе, перед носом коробля, имеется палуба другого корабля
        if (beforeStartIndex >= 0
                && (collisionChecker.test(beforeStartIndex) || borderChecker.test(beforeStartIndex))) {
            // Проверка не пройдена
            return false;
        }
        // Номер индекса после конечного
        final int afterEndIndex = endIndex + 1;
        // Если на границе, за хвостом коробля, имеется палуба другого корабля
        if (afterEndIndex < (isHorizontal ? columns : rows)
                && (collisionChecker.test(afterEndIndex) || borderChecker.test(afterEndIndex))) {
            // Проверка не пройдена
            return false;
        }
        // Проверка пройдена
        return true;
    }
}
