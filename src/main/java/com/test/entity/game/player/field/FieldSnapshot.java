package com.test.entity.game.player.field;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.entity.game.player.field.cell.CellSnapshot;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Снимок игрового поля
 * <p>
 * Это POJO, необходимый для маршалинга объекта с данными о поле {@link Field} в JSON-структуру
 * После маршалинга JSON-структура должна выглядеть следующим образом:
 * {"cells": [
 * [{"row": "0","column": "a","content": "DECK"}, ..., {"row": "0","column": "j","content": "EMPTINESS"}]
 * ...
 * [{"row": "9","column": "a","content": "EMPTINESS"}, ..., {"row": "9","column": "j","content": "DESTROYED_DECK"}]
 * ]}
 */
@JsonInclude(NON_NULL)
public class FieldSnapshot {
    @JsonProperty
    private CellSnapshot[][] cells; // Массив ячеек

    /**
     * @return массив ячеек {@link CellSnapshot}
     */
    public CellSnapshot[][] getCells() {
        return cells;
    }

    /**
     * Установить массив ячеек
     *
     * @param cells массив ячеек {@link CellSnapshot}
     */
    public void setCells(CellSnapshot[][] cells) {
        this.cells = cells;
    }
}
