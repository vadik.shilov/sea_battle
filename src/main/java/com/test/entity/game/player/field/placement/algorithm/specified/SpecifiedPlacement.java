package com.test.entity.game.player.field.placement.algorithm.specified;

import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.placement.algorithm.PlacementAlgorithm;
import com.test.entity.game.player.field.ship.Ship;
import org.springframework.util.Assert;

/**
 * Класс, выполняющий предопределенное размещение кораблей на игровом поле
 * <p>
 * После размещения корблей поле будет выглядеть следующим образом:
 * <p>
 * o--ooo---o
 * o--------o
 * o---------
 * o----oo--o
 * ----------
 * ---o------
 * ---o--o---
 * ----------
 * ----------
 * -o-o--ooo-
 * <p>
 * Где символ "-" - пустая ячейка, "o" - палуба корабля
 */
public class SpecifiedPlacement implements PlacementAlgorithm {
    @Override
    public void place(Cell[][] cells, Ship[] ships) {
        // Проверяем ячееки на null
        Assert.notNull(cells);
        // Проверяем корабли на null
        Assert.noNullElements(ships);

        // Счетчик количества размещенных четырехпалубных кораблей
        int fourDecksShips = 0;
        // Счетчик количества размещенных трехпалубных кораблей
        int treeDecksShips = 0;
        // Счетчик количества размещенных двухпалубных кораблей
        int twoDecksShips = 0;
        // Счетчик количества размещенных однопалубных кораблей
        int oneDeckShips = 0;

        // Перебираем массив кораблей
        for (Ship ship : ships) {
            if (ship.getSize() == 4) {
                // Размещаем четырехпалубный корабль
                placeFourDecksShip(cells, ship, fourDecksShips);
                // Увеличиваем счетчик количества размещенных четырехпалубных кораблей
                fourDecksShips++;
            } else if (ship.getSize() == 3) {
                // Размещаем трехпалубный корабль
                placeTreeDecksShip(cells, ship, treeDecksShips);
                // Увеличиваем ччетчик количества размещенных трехпалубных кораблей
                treeDecksShips++;
            } else if (ship.getSize() == 2) {
                // Размещаем двухпалубный корабль
                placeTwoDecksShip(cells, ship, twoDecksShips);
                // Увеличиваем счетчик количества размещенных двухпалубных кораблей
                twoDecksShips++;
            } else if (ship.getSize() == 1) {
                // Размещаем однопалубный корабль
                placeOneDeckShip(cells, ship, oneDeckShips);
                // Увеличиваем счетчик количества размещенных однопалубных кораблей
                oneDeckShips++;
            } else {
                // Выбрасываем исключение, если у корабля не 1, не 2, не 3 и не 4 палубы
                throw new IllegalStateException("Unexpected size of ship: " + ship.getSize() + "!");
            }
        }
    }

    /**
     * Расположить четырехпалубный корабль
     *
     * @param cells массив ячееек {@link Cell}
     * @param ship  корабль {@link Ship}
     * @param count количество уже расположенных на игровом поле четырехпалубных кораблей
     */
    private void placeFourDecksShip(Cell[][] cells, Ship ship, int count) {
        if (count == 0) {
            // Размещаем единственный четырехпалубный корабль
            cells[0][0].setDeck(ship.getDecks()[0]);
            cells[1][0].setDeck(ship.getDecks()[1]);
            cells[2][0].setDeck(ship.getDecks()[2]);
            cells[3][0].setDeck(ship.getDecks()[3]);
        } else {
            // Выбрасываем исключение, если количество четырехпалубных кораблей > 1
            throw new IllegalStateException("Unexpected count of ships: " + count + "!");
        }
    }

    /**
     * Расположить трехпалубный корабль
     *
     * @param cells массив ячееек {@link Cell}
     * @param ship  корабль {@link Ship}
     * @param count количество уже расположенных на игровом поле трехпалубных кораблей
     */
    private void placeTreeDecksShip(Cell[][] cells, Ship ship, int count) {
        if (count == 0) {
            // Размещаем первый трехпалубный корабль
            cells[0][3].setDeck(ship.getDecks()[0]);
            cells[0][4].setDeck(ship.getDecks()[1]);
            cells[0][5].setDeck(ship.getDecks()[2]);
        } else if (count == 1) {
            // Размещаем второй трехпалубный корабль
            cells[9][6].setDeck(ship.getDecks()[0]);
            cells[9][7].setDeck(ship.getDecks()[1]);
            cells[9][8].setDeck(ship.getDecks()[2]);
        } else {
            // Выбрасываем исключение, если количество трехпалубных кораблей > 2
            throw new IllegalStateException("Unexpected count of ships: " + count + "!");
        }
    }

    /**
     * Расположить двухпалубный корабль
     *
     * @param cells массив ячееек {@link Cell}
     * @param ship  корабль {@link Ship}
     * @param count количество уже расположенных на игровом поле двухпалубных кораблей
     */
    private void placeTwoDecksShip(Cell[][] cells, Ship ship, int count) {
        if (count == 0) {
            // Размещаем первый двухпалубный корабль
            cells[0][9].setDeck(ship.getDecks()[0]);
            cells[1][9].setDeck(ship.getDecks()[1]);
        } else if (count == 1) {
            // Размещаем второй двухпалубный корабль
            cells[3][5].setDeck(ship.getDecks()[0]);
            cells[3][6].setDeck(ship.getDecks()[1]);
        } else if (count == 2) {
            // Размещаем третий двухпалубный корабль
            cells[5][3].setDeck(ship.getDecks()[0]);
            cells[6][3].setDeck(ship.getDecks()[1]);
        } else {
            // Выбрасываем исключение, если количество двухпалубных кораблей > 3
            throw new IllegalStateException("Unexpected count of ships: " + count + "!");
        }
    }

    /**
     * Расположить однопалубный корабль
     *
     * @param cells массив ячееек {@link Cell}
     * @param ship  корабль {@link Ship}
     * @param count количество уже расположенных на игровом поле однопалубных кораблей
     */
    private void placeOneDeckShip(Cell[][] cells, Ship ship, int count) {
        if (count == 0) {
            // Размещаем первый однопалубный корабль
            cells[3][9].setDeck(ship.getDecks()[0]);
        } else if (count == 1) {
            // Размещаем второй однопалубный корабль
            cells[6][6].setDeck(ship.getDecks()[0]);
        } else if (count == 2) {
            // Размещаем третий однопалубный корабль
            cells[9][1].setDeck(ship.getDecks()[0]);
        } else if (count == 3) {
            // Размещаем четвертый однопалубный корабль
            cells[9][3].setDeck(ship.getDecks()[0]);
        } else {
            // Выбрасываем исключение, если количество однопалубных кораблей > 4
            throw new IllegalStateException("Unexpected count of ships: " + count + "!");
        }
    }
}
