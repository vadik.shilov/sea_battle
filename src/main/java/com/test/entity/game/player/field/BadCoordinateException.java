package com.test.entity.game.player.field;

import com.test.SeaBattleException;

/**
 * Некорректная координата
 */
public class BadCoordinateException extends SeaBattleException {
    private final int coordinate; // Координата
    private final boolean isRow; // Флаг, указывающий на то, что это координата строки

    /**
     * @param coordinate координата
     * @param isRow      флаг, указывающий на то, что это координата строки
     */
    public BadCoordinateException(int coordinate, boolean isRow) {
        this.coordinate = coordinate;
        this.isRow = isRow;
    }

    /**
     * @return координата
     */
    public int getCoordinate() {
        return coordinate;
    }

    /**
     * @return флаг, указывающий на то, что это координата строки
     */
    public boolean isRow() {
        return isRow;
    }
}
