package com.test.entity.game.player.field.placement.algorithm.random;

/**
 * Итератор возможных вариантов расположение корабля на игровом поле
 */
public class PositionIterator {
    private final int size; // Количество палуб
    private final CoordinateIterator row; // Возможные значения для номера строки
    private final CoordinateIterator column; // Возможные значения для номера столбца
    private final CoordinateIterator angle; // Возможные значения для угла

    /**
     * @param size    количество палуб
     * @param rows    количество строк у игрового поля
     * @param columns количество столбцов у игрового поля
     */
    PositionIterator(int size, int rows, int columns) {
        this.size = size;
        row = new CoordinateIterator(createAndFillArray(rows));
        column = new CoordinateIterator(createAndFillArray(columns));
        angle = new CoordinateIterator(createAndFillArray(2));
    }

    /**
     * Создать и заполнить массив возможных значений
     *
     * @param size размер массива
     * @return массив возможных значений
     */
    private int[] createAndFillArray(int size) {
        // Создаем массив
        final int[] arr = new int[size];
        // Заполняем массив
        for (int i = 0; i < size; i++) {
            // Помещаем значение в массив
            arr[i] = i;
        }
        // Возвращаем массив
        return arr;
    }

    /**
     * @return количество палуб
     */
    public int getSize() {
        return size;
    }

    /**
     * @return номер строки
     */
    public int getRow() {
        return row.get();
    }

    /**
     * @return номер столбца
     */
    public int getColumn() {
        return column.get();
    }

    /**
     * @return угол
     */
    public int getAngle() {
        return angle.get();
    }

    /**
     * @return {@code true}, если имеется следующий вариант расположения
     */
    boolean hasNext() {
        return angle.hasNext() || row.hasNext() || column.hasNext();
    }

    /**
     * Перейти к следующему варианту расположения
     *
     * @throws IllegalStateException если следующего варианта расположения нет
     */
    void next() {
        if (angle.hasNext()) {
            // Инкрементируем итератора итератора
            angle.next();
        } else if (row.hasNext()) {
            // Инкрементируем итератора итератора
            row.next();
            // Сбрасываем индекс итератора
            angle.reset();
        } else if (column.hasNext()) {
            // Инкрементируем итератора итератора
            column.next();
            // Сбрасываем индекс итератора
            row.reset();
            // Сбрасываем индекс итератора
            angle.reset();
        } else {
            // Выбрасываем исключение
            throw new IllegalStateException("Couldn't generate ship positions for field!");
        }
    }

    /**
     * Сбросить индекс итератора
     */
    void reset() {
        row.reset();
        column.reset();
        angle.reset();
    }
}
