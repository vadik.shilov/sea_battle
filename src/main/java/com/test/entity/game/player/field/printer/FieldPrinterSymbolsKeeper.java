package com.test.entity.game.player.field.printer;

import org.springframework.util.Assert;

/**
 * Хранитель символов, необходимых для печати игрового поля
 */
public class FieldPrinterSymbolsKeeper {
    private final String notAttackedCellSymbol; // Символ для ячейки, в которую еще не производился выстрел
    private final String attackedCellSymbol; // Символ для ячейки, в которую производился выстрел
    private final String deckSymbol; // Символ для ячейки с дееспособной палубой
    private final String destroyedDeckSymbol; // Символ для ячейки с уничтоженной палубой

    /**
     * @param notAttackedCellSymbol символ для ячейки, в которую еще не производился выстрел
     * @param attackedCellSymbol    символ для ячейки, в которую производился выстрел
     * @param deckSymbol            символ для ячейки с дееспособной палубой
     * @param destroyedDeckSymbol   символ для ячейки с уничтоженной палубой
     * @throws IllegalArgumentException если один из входных атрибутов = {@code null} или пустой строке
     */
    public FieldPrinterSymbolsKeeper(
            String notAttackedCellSymbol,
            String attackedCellSymbol,
            String deckSymbol,
            String destroyedDeckSymbol
    ) {
        // Проверяем входные атрибуты на null и ""
        Assert.hasText(notAttackedCellSymbol);
        Assert.hasText(attackedCellSymbol);
        Assert.hasText(deckSymbol);
        Assert.hasText(destroyedDeckSymbol);
        // Устанавливаем символы
        this.notAttackedCellSymbol = notAttackedCellSymbol;
        this.attackedCellSymbol = attackedCellSymbol;
        this.deckSymbol = deckSymbol;
        this.destroyedDeckSymbol = destroyedDeckSymbol;
    }

    /**
     * @return символ для ячейки, в которую еще не производился выстрел
     */
    String getNotAttackedCellSymbol() {
        return notAttackedCellSymbol;
    }

    /**
     * @return символ для ячейки, в которую производился выстрел
     */
    String getAttackedCellSymbol() {
        return attackedCellSymbol;
    }

    /**
     * @return символ для ячейки с дееспособной палубой
     */
    String getDeckSymbol() {
        return deckSymbol;
    }

    /**
     * @return символ для ячейки с уничтоженной палубой
     */
    String getDestroyedDeckSymbol() {
        return destroyedDeckSymbol;
    }
}
