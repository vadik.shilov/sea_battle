package com.test.entity.game.player.field.converter;

import com.test.SeaBattleException;

/**
 * Некорректный псевдоним столбца игрового поля
 */
public class BadColumnAliasException extends SeaBattleException {
    private final String alias; // Псевдоним

    /**
     * @param alias псевдоним
     */
    BadColumnAliasException(String alias) {
        this.alias = alias;
    }

    /**
     * @return псевдоним
     */
    public String getAlias() {
        return alias;
    }
}
