package com.test.entity.game.player;

import com.test.entity.game.player.field.Field;
import com.test.entity.game.player.field.ship.Ship;
import org.springframework.util.Assert;

/**
 * Игрок
 */
public class Player {
    /**
     * Возможные статусы игрока
     */
    public enum State {
        IS_NOT_READY, // Не готов к игре
        IS_READY, // Готов к игре
        IS_READY_FOR_RESTART, // Играет, но готов к перезапуску игры
        IS_PLAYING; // Играет

        /**
         * Проверить, что статус содержится в массиве статусов
         *
         * @param state  проверяемый статус
         * @param states массив статусов в котором будет произведен поиск
         * @return {@code true}, если статус содержится в массиве
         */
        public static boolean in(State state, State... states) {
            if (states != null) {
                for (State s : states) {
                    if (s == state) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private String name; // Наименование
    private State state; // Статус
    private Ship[] ships; // Корабли
    private Field field; // Игровое поле

    /**
     * @return наименование
     */
    public String getName() {
        return name;
    }

    /**
     * Задать наименование
     *
     * @param name наименование {@link State}
     * @throws IllegalArgumentException если в качестве наименования передан {@code null} или пустая строка
     */
    public void setName(String name) {
        // Проверяем наименование на null и ""
        Assert.hasText(name);
        // Задаем имя
        this.name = name;
    }

    /**
     * @return статус {@link State}
     */
    public State getState() {
        return state;
    }

    /**
     * Задать статус
     *
     * @param state статус {@link State}
     * @throws IllegalArgumentException если в качестве статуса передан {@code null}
     */
    public void setState(State state) {
        // Проверяем статус на null
        Assert.notNull(state);
        // Задаем статус
        this.state = state;
    }

    /**
     * @return корабли {@link State}
     */
    public Ship[] getShips() {
        return ships;
    }

    /**
     * Задать корабли
     *
     * @param ships массив кораблей {@link Ship}
     * @throws IllegalArgumentException если переданный массив кораблей или один из его элементов = {@code null}
     */
    public void setShips(Ship... ships) {
        // Проверяем, что массив не null и не содержит null элементов
        Assert.noNullElements(ships);
        // Задаем массив кораблей
        this.ships = ships;
    }

    /**
     * @return игровое поле {@link Field}
     */
    public Field getField() {
        return field;
    }

    /**
     * Установить игровое поле
     *
     * @param field игровое поле {@link Field}
     * @throws IllegalArgumentException если в качестве игрового поля передан {@code null}
     */
    public void setField(Field field) {
        // Проверяем поле на null
        Assert.notNull(field);
        // Устанавливаем поле
        this.field = field;
    }
}
