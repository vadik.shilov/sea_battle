package com.test.entity.game.player.field.placement.algorithm;

import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.ship.Ship;

/**
 * Алгоритм размещения короблей на игровом поле
 */
public interface PlacementAlgorithm {
    /**
     * Разместить корабли
     *
     * @param cells массив ячееек {@link Cell}
     * @param ships массив кораблей {@link Ship}
     * @throws IllegalArgumentException если хотя бы один из переданных массивов или их элементов = {@code null}
     */
    void place(Cell[][] cells, Ship[] ships);
}
