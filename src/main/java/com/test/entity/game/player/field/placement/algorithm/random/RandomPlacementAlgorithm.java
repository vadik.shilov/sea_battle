package com.test.entity.game.player.field.placement.algorithm.random;

import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.placement.algorithm.PlacementAlgorithm;
import com.test.entity.game.player.field.ship.Ship;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Класс, выполняющий расположение кораблей рандомным образом
 * Описание алгоритма доступно по ссылке https://habrahabr.ru/post/186730/
 */
public class RandomPlacementAlgorithm implements PlacementAlgorithm {
    @Override
    public void place(Cell[][] cells, Ship[] ships) {
        // Проверяем ячееки на null
        Assert.notNull(cells);
        // Проверяем корабли на null
        Assert.noNullElements(ships);
        // Создаем генератор
        final ShipsPositionsGenerator generator = new ShipsPositionsGenerator(cells.length, cells[0].length);
        // Генерируем данные для расположения кораблей
        final PositionIterator[] positions = generator.generatePositions();
        // Создаем список для кораблей
        final List<Ship> shipsList = new ArrayList<>();
        // Заполняем список данными из массива
        Collections.addAll(shipsList, ships);
        // Перебираем
        for (PositionIterator position : positions) {
            // Извлекаем корабль с нужным количеством палуб из списка
            final Ship ship = poolShipBySize(shipsList, position.getSize());
            // Помещаем палубы корабля в массив ячеек
            putShipIntoCells(ship, position, cells);
        }
    }

    /**
     * Поместить палубы корабля в массив ячеек
     *
     * @param ship     корабль {@link Ship}
     * @param position позиция корабля {@link PositionIterator}
     * @param cells    массив ячеек {@link Cell}
     */
    private void putShipIntoCells(Ship ship, PositionIterator position, Cell[][] cells) {
        int deckIndex = 0;
        if (position.getAngle() == 0) {
            for (int i = position.getColumn(); i < position.getColumn() + position.getSize(); i++) {
                cells[position.getRow()][i].setDeck(ship.getDecks()[deckIndex++]);
            }
        } else {
            for (int i = position.getRow(); i < position.getRow() + position.getSize(); i++) {
                cells[i][position.getColumn()].setDeck(ship.getDecks()[deckIndex++]);
            }
        }
    }

    /**
     * Извлечь корабль с нужным количеством палуб из списка
     *
     * @param ships список кораблей
     * @param size  количеством палуб
     * @return корабль {@link Ship}
     * @throws IllegalStateException если корабль с указанным количеством палуб отсутствует в списке
     */
    private Ship poolShipBySize(List<Ship> ships, int size) {
        for (Ship ship : ships) {
            if (ship.getSize() == size) {
                ships.remove(ship);
                return ship;
            }
        }
        throw new IllegalStateException("Couldn't find a ship!");
    }
}
