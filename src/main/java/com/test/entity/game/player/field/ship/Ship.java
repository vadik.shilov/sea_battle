package com.test.entity.game.player.field.ship;


import com.test.entity.game.player.field.ship.deck.Deck;
import org.springframework.util.Assert;

/**
 * Корабль
 */
public class Ship {
    private Deck[] decks; // Палубы

    /**
     * @return палубы {@link Deck}
     */
    public Deck[] getDecks() {
        return decks;
    }

    /**
     * Установить палубы
     *
     * @param decks палубы {@link Deck}
     * @throws IllegalArgumentException если переданый массив палуб или один из его элементов = {@code null}
     */
    public void setDecks(Deck... decks) {
        // Проверяем, что массив не null и не содержит null элементов
        Assert.noNullElements(decks);
        // Устанавливаем кораблю палубы
        this.decks = decks;
        // Перебираем элементы массива
        for (Deck deck : decks) {
            // Устанавливаем палубе корабль
            deck.setShip(this);
        }
    }

    /**
     * @return количество палуб
     */
    public int getSize() {
        return decks.length;
    }

    /**
     * @return {@code true}, если корабль уничтожен
     */
    public boolean isDestroyed() {
        // Перебираем все палубы корабля
        for (Deck deck : decks) {
            // Если хотя бы одна палуба в целости, то корабль еще дееспособен
            if (!deck.isDestroyed()) {
                return false;
            }
        }
        // Иначе корабль разрушен
        return true;
    }
}
