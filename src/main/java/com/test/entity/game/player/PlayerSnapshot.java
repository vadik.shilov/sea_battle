package com.test.entity.game.player;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.entity.game.player.field.FieldSnapshot;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Снимок информации об игроке
 * <p>
 * Это POJO, необходимый для маршалинга объекта с данными об игроке {@link Player} в JSON-структуру
 * После маршалинга JSON-структура должна выглядеть следующим образом:
 * {"state: "статус", "name": "имя игрока", "field": {...}}
 */
@JsonInclude(NON_NULL)
public class PlayerSnapshot {
    @JsonProperty
    private Player.State state; // Статус
    @JsonProperty
    private String name; // Наименование
    @JsonProperty
    private FieldSnapshot field; // Игровое поле

    /**
     * @return статус {@link Player.State}
     */
    public Player.State getState() {
        return state;
    }

    /**
     * Задать статус
     *
     * @param state статус {@link Player.State}
     */
    public void setState(Player.State state) {
        this.state = state;
    }

    /**
     * @return наименование
     */
    public String getName() {
        return name;
    }

    /**
     * Задать наименование
     *
     * @param name наименование
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return игровое поле {@link FieldSnapshot}
     */
    public FieldSnapshot getField() {
        return field;
    }

    /**
     * Установить игровое поле
     *
     * @param field распечатка игрового поля {@link FieldSnapshot}
     */
    public void setField(FieldSnapshot field) {
        this.field = field;
    }
}
