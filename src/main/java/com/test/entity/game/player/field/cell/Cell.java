package com.test.entity.game.player.field.cell;

import com.test.entity.game.player.field.BadCoordinateException;
import com.test.entity.game.player.field.CellAlreadyAttackedException;
import com.test.entity.game.player.field.ship.deck.Deck;
import org.springframework.util.Assert;

/**
 * Ячейка игрового поля
 */
public class Cell {
    private int row; // Номер строки
    private int column; // Номер столбца
    private boolean attacked; // Флаг, указывающий на то, что в ячейку был произведен выстрел
    private Deck deck; // Палуба корабля

    /**
     * @return {@code true}, если в ячейку был произведен выстрел
     */
    public boolean isAttacked() {
        return attacked;
    }

    /**
     * @return номер строки
     */
    public int getRow() {
        return row;
    }

    /**
     * Задать номер строки
     *
     * @param row номер строки
     * @throws BadCoordinateException если номер строки < 0
     */
    public void setRow(int row) {
        // Проверяем, что номер строки >= 0
        if (row < 0) {
            throw new BadCoordinateException(row, true);
        }
        // задаем номер строки
        this.row = row;
    }

    /**
     * @return номер столбца
     */
    public int getColumn() {
        return column;
    }

    /**
     * Задать номер столбца
     *
     * @param column номер столбца
     * @throws BadCoordinateException если номер столбца < 0
     */
    public void setColumn(int column) {
        // Проверяем, что номер столбца >= 0
        if (column < 0) {
            throw new BadCoordinateException(column, false);
        }
        // задаем номер столбца
        this.column = column;
    }

    /**
     * Произвести выстрел в ячейку
     *
     * @return уничтоженная палуба корабля {@link Deck}, если ячейка ее содержит, иначе {@code null}
     * @throws CellAlreadyAttackedException если в указанную ячейку выстрел уже производился
     */
    public Deck attack() {
        // Проверяем, что в ячейку выстрел еще не был произведен
        if (attacked) {
            throw new CellAlreadyAttackedException(row, column);
        }
        // Совершаем выстрел в ячейку
        attacked = true;
        // Если ячейка содержит палубу корабля
        if (deck != null) {
            // Уничтожаем палубу корабля
            deck.destroy();
        }
        // Возвращаем уничтоженную палубу корабля (null, если она не задана)
        return deck;
    }

    /**
     * @return {@code true}, если ячейка содержит палубу корабля
     */
    public boolean hasDeck() {
        return deck != null;
    }

    /**
     * @return палуба корабля {@link Deck}, если ячейка ее содержит, иначе {@code null}
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Установить палубу корабля
     *
     * @param deck палуба корабля {@link Deck}
     * @throws IllegalArgumentException если в качестве палубы корабля передан {@code null}
     */
    public void setDeck(Deck deck) {
        // Проверяем палубу на null
        Assert.notNull(deck);
        // Устанавливаем палубу в ячейку
        this.deck = deck;
    }
}
