package com.test.entity.game.player.field.printer;

import com.test.entity.game.player.field.Field;
import com.test.entity.game.player.field.cell.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * Принтер для печати игрового поля
 */
public class FieldPrinter {
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldPrinter.class); // Логгер
    private final FieldPrinterSymbolsKeeper symbolsKeeper; // Хранитель символов, необходимых для печати

    /**
     * @param symbolsKeeper хранитель символов {@link FieldPrinterSymbolsKeeper}
     * @throws IllegalArgumentException если в качестве хранителя символов передан {@code null}
     */
    public FieldPrinter(FieldPrinterSymbolsKeeper symbolsKeeper) {
        // Проверяем хранитель символов на null
        Assert.notNull(symbolsKeeper);
        // Устанавливаем хранитель символов
        this.symbolsKeeper = symbolsKeeper;
    }

    /**
     * Получить распечатку игрового поля
     *
     * @param field игровое поле
     * @throws IllegalArgumentException если в качестве игрового поля передан {@code null}
     */
    public void printField(Field field) {
        // Проверяем игровое поле на null
        Assert.notNull(field);
        // Создаем строковый буффер
        final StringBuilder sb = new StringBuilder("\r\n");
        // Перебираем ячейки массива
        for (Cell[] cellsRow : field.getCells()) {
            for (Cell cell : cellsRow) {
                // Записываем в буффер распечатанную ячейку массива
                sb.append(printCell(cell));
            }
            // Добавляем символ переноса строки
            sb.append("\r\n");
        }
        // Печатаем результат из буффера
        LOGGER.info(sb.toString());
    }

    /**
     * Получить распечатку ячейки игрового поля
     *
     * @param cell ячейка игрового поля
     * @return распечатка ячейки
     * @throws IllegalArgumentException если в качестве ячейки игрового поля передан {@code null}
     */
    private String printCell(Cell cell) {
        // Проверяем ячейку на null
        Assert.notNull(cell);
        // Если ячейка содержит палубу корабля
        if (cell.hasDeck()) {
            // Если в ячейку производился выстрел
            if (cell.isAttacked()) {
                // Возвращаем символ для ячейки с уничтоженной палубой
                return symbolsKeeper.getDestroyedDeckSymbol();
            }
            // Возвращаем символ для ячейки с дееспособной палубой
            return symbolsKeeper.getDeckSymbol();
        }
        // Если в ячейку производился выстрел
        if (cell.isAttacked()) {
            // Возвращаем символ для ячейки, в которую производился выстрел
            return symbolsKeeper.getAttackedCellSymbol();
        }
        // Возвращаем символ для ячейки, в которую еще не производился выстрел
        return symbolsKeeper.getNotAttackedCellSymbol();
    }
}
