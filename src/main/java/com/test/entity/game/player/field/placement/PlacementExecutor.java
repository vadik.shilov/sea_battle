package com.test.entity.game.player.field.placement;

import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.placement.algorithm.PlacementAlgorithm;
import com.test.entity.game.player.field.ship.Ship;

/**
 * Класс, выполняющий размещение кораблей на игровом поле при помощи указанного алгоритма
 * Реализация паттерна "Стратегия"
 */
public class PlacementExecutor {
    private final PlacementAlgorithm algorithm; // Алгоритм размещения

    public PlacementExecutor(PlacementAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * @return алгоритм размещения {@link PlacementAlgorithm}
     */
    public PlacementAlgorithm getAlgorithm() {
        return algorithm;
    }

    /**
     * Разместить корабли при помощи указанного алгоритма
     *
     * @param cells массив ячееек {@link Cell}
     * @param ships массив кораблей {@link Ship}
     */
    public void execute(Cell[][] cells, Ship[] ships) {
        // Размещаем корабли при помощи указанного алгоритма
        algorithm.place(cells, ships);
    }
}
