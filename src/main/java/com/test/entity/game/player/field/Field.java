package com.test.entity.game.player.field;

import com.test.entity.game.player.field.cell.Cell;
import org.springframework.util.Assert;

/**
 * Игровове поле
 */
public class Field {
    private Cell[][] cells; // Массив ячеек

    /**
     * Установить массив ячеек
     *
     * @param cells массив ячеек {@link Cell}
     * @throws IllegalArgumentException если в качестве массива ячеек передан {@code null}
     */
    public void setCells(Cell[][] cells) {
        // Проверяем массив ячеек на null
        Assert.notNull(cells);
        // Устанавливаем массив ячеек
        this.cells = cells;
    }

    /**
     * @return массив ячеек {@link Cell}
     */
    public Cell[][] getCells() {
        return cells;
    }

    /**
     * Получить ячейку с указанными координатами
     *
     * @param row    номер строки
     * @param column номер столбца
     * @return ячейка {@link Cell}
     * @throws BadCoordinateException если передана некорректная координата
     */
    public Cell getCell(int row, int column) {
        // Проверяем координаты ячейки на корректность
        checkCellCoordinates(row, column);
        // Возвращаем ячейку с указанными координатами
        return cells[row][column];
    }

    /**
     * Проверка координат ячейки на корректность
     *
     * @param row    номер строки
     * @param column номер столбца
     * @throws BadCoordinateException если передана некорректная координата
     */
    private void checkCellCoordinates(int row, int column) {
        // Проверка строки
        if (row < 0 || row >= cells.length) {
            throw new BadCoordinateException(row, true);
        }
        // Проверка столбца
        if (column < 0 || column >= cells[row].length) {
            throw new BadCoordinateException(column, false);
        }
    }
}
