package com.test.entity.game.player.field.cell;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Снимок ячейки игрового поля
 * <p>
 * Это POJO, необходимый для маршалинга объекта с данными о ячейке {@link Cell} в JSON-структуру
 * После маршалинга JSON-структура должна выглядеть следующим образом:
 * {"row: "7", "column": "d", "content": "DESTROYED_DECK"}
 */
@JsonInclude(NON_NULL)
public class CellSnapshot {
    public enum Content {
        DECK, // Дееспособная палуба корабля
        DESTROYED_DECK, // Уничтоженная палуба корабля
        EMPTINESS, // Пустота
        MISS, // Промах
        SECRET, // Содержание не известно
    }

    @JsonProperty
    private String row; // Строка
    @JsonProperty
    private String column; // Столбец
    @JsonProperty
    private Content content; // Содержимое

    /**
     * @return строка
     */
    public String getRow() {
        return row;
    }

    /**
     * Задать строку
     *
     * @param row строка
     */
    public void setRow(String row) {
        this.row = row;
    }

    /**
     * @return столбец
     */
    public String getColumn() {
        return column;
    }

    /**
     * Задать столбец
     *
     * @param column столбец
     */
    public void setColumn(String column) {
        this.column = column;
    }

    /**
     * @return содержимое {@link Content}
     */
    public Content getContent() {
        return content;
    }

    /**
     * Задать содержимое
     *
     * @param content содержимое {@link Content}
     */
    public void setContent(Content content) {
        this.content = content;
    }
}
