package com.test.entity.game.player.field.placement.algorithm.random;

import java.util.Random;

/**
 * Итератор возможных значений координаты
 */
class CoordinateIterator {
    private final int arr[]; // Массив элементов
    private int index; // Индекс

    /**
     * @param arr массив возможных значений координаты
     */
    CoordinateIterator(int[] arr) {
        this.arr = arr;
        shuffle();
    }

    /**
     * @return координата для текущего индекса
     */
    int get() {
        return arr[index];
    }

    /**
     * @return {@code true}, если имеется следующая возможная координата
     */
    boolean hasNext() {
        return index < arr.length - 1;
    }

    /**
     * Перейти к следующей возможной координате
     *
     * @throws ArrayIndexOutOfBoundsException если следующей возможной координаты нет
     */
    void next() {
        // Если следующая возможная координата отсутствует
        if (!hasNext()) {
            // Выбрасываем исключение
            throw new ArrayIndexOutOfBoundsException(index + 1);
        }
        // Инкрементируем индекс
        index++;
    }

    /**
     * Сбросить индекс итератора
     */
    void reset() {
        index = 0;
    }

    /**
     * Размешиваем массив
     */
    private void shuffle() {
        final Random random = new Random();
        // Размешиваем массив столько раз, сколько у него размерность
        for (int n = 0; n < arr.length; n++) {
            // Формула гарантирует, что будут определены два разных индекса массива
            final int i = random.nextInt(arr.length) % arr.length;
            final int j = (i + 1 + arr[random.nextInt(arr.length)] % (arr.length - 1)) % arr.length;
            // Меняем местами элементы массива
            arr[i] += arr[j];
            arr[j] = arr[i] - arr[j];
            arr[i] -= arr[j];
        }
    }
}
