package com.test.entity.game;

import com.test.SeaBattleException;

/**
 * Некорректный индекс ячейки
 */
public class BadCellIndexException extends SeaBattleException {
    private final String cellIndex; // индекс ячейки

    /**
     * @param cellIndex индекс ячейки
     */
    BadCellIndexException(String cellIndex) {
        this.cellIndex = cellIndex;
    }

    /**
     * @return индекс ячейки
     */
    public String getCellIndex() {
        return cellIndex;
    }
}
