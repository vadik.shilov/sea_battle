package com.test.entity.game;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.test.entity.game.player.PlayerSnapshot;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Снимок информации об игре
 * <p>
 * Это POJO, необходимый для маршалинга объекта с данными об игре {@link Game} в JSON-структуру
 * После маршалинга JSON-структура должна выглядеть следующим образом:
 * {
 * "state": "статус игры",
 * "player1" : {"state: "статус", "name": "имя игрока", "field": {...}},
 * "player2" : {"state": "статус", "name": "имя игрока", "field": {...}}
 * }
 */
@JsonInclude(NON_NULL)
public class GameSnapshot {
    @JsonProperty
    private Game.State state; // Статус
    @JsonProperty
    private PlayerSnapshot player1; // Информация об игроке 1
    @JsonProperty
    private PlayerSnapshot player2; // Информация об игроке 2

    /**
     * @return статус {@link Game.State}
     */
    public Game.State getState() {
        return state;
    }

    /**
     * Задать статус
     *
     * @param state статус {@link Game.State}
     */
    void setState(Game.State state) {
        this.state = state;
    }

    /**
     * @return снимок информация об игроке 1 {@link PlayerSnapshot}
     */
    public PlayerSnapshot getPlayer1() {
        return player1;
    }

    /**
     * Задать снимок информация об игроке 1
     *
     * @param player1 снимок информация об игроке 1 {@link PlayerSnapshot}
     */
    void setPlayer1(PlayerSnapshot player1) {
        this.player1 = player1;
    }

    /**
     * @return снимок информация об игроке 2 {@link PlayerSnapshot}
     */
    public PlayerSnapshot getPlayer2() {
        return player2;
    }

    /**
     * Задать снимок информация об игроке 2
     *
     * @param player2 снимок информация об игроке 2 {@link PlayerSnapshot}
     */
    void setPlayer2(PlayerSnapshot player2) {
        this.player2 = player2;
    }
}
