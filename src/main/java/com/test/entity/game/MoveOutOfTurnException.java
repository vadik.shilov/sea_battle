package com.test.entity.game;

import com.test.SeaBattleException;

/**
 * Ход вне очереди
 */
public class MoveOutOfTurnException extends SeaBattleException {
    private final String playerName; // Наименование игрока

    /**
     * @param playerName наименование игрока
     */
    MoveOutOfTurnException(String playerName) {
        this.playerName = playerName;
    }

    /**
     * @return наименование игрока
     */
    public String getPlayerName() {
        return playerName;
    }
}
