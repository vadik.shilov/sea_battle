package com.test.entity.game;

import com.test.SeaBattleException;

/**
 * Недопустимое действие
 */
public class IllegalActionException extends SeaBattleException {
}
