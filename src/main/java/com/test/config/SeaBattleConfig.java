package com.test.config;

import com.test.entity.game.Game;
import com.test.entity.game.GameSnapshot;
import com.test.entity.game.player.Player;
import com.test.entity.game.player.PlayerSnapshot;
import com.test.entity.game.player.field.Field;
import com.test.entity.game.player.field.FieldSnapshot;
import com.test.entity.game.player.field.cell.Cell;
import com.test.entity.game.player.field.cell.CellSnapshot;
import com.test.entity.game.player.field.converter.ColumnIndexConverter;
import com.test.entity.game.player.field.placement.PlacementExecutor;
import com.test.entity.game.player.field.placement.algorithm.PlacementAlgorithm;
import com.test.entity.game.player.field.placement.algorithm.random.RandomPlacementAlgorithm;
import com.test.entity.game.player.field.placement.algorithm.specified.SpecifiedPlacement;
import com.test.entity.game.player.field.printer.FieldPrinter;
import com.test.entity.game.player.field.printer.FieldPrinterSymbolsKeeper;
import com.test.entity.game.player.field.ship.Ship;
import com.test.entity.game.player.field.ship.deck.Deck;
import com.test.filter.PlayerNameCheckerFilter;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;

import java.util.stream.Stream;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

/**
 * Конфигурация бинов
 */
@Configuration
@PropertySource("classpath:config/config.properties")
public class SeaBattleConfig {
    @Autowired
    private ApplicationContext context; // Контекст приложения
    @Autowired
    private Environment environment; // Конфигурация приложения

    /**
     * @return фильтр, необходимый для проверки имени игрока {@link PlayerNameCheckerFilter}
     */
    @Bean
    public PlayerNameCheckerFilter playerNameCheckerFilter() {
        return new PlayerNameCheckerFilter();
    }

    /**
     * @return бин для регистрации фильтра playerNameCheckerFilter {@link FilterRegistrationBean}
     * @see PlayerNameCheckerFilter
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        final FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.addUrlPatterns("/game/*");
        registration.setFilter(playerNameCheckerFilter());
        return registration;
    }

    /**
     * @return бин, выполняющий размещение кораблей на игровом поле рандомным образом {@link PlacementAlgorithm}
     * @see RandomPlacementAlgorithm
     */
    @Bean
    public PlacementAlgorithm randomPlacementAlgorithm() {
        return new RandomPlacementAlgorithm();
    }

    /**
     * @return бин, выполняющий предопределенное размещение кораблей на игровом поле {@link PlacementAlgorithm}
     * @see SpecifiedPlacement
     */
    @Bean
    public PlacementAlgorithm specifiedPlacementAlgorithm() {
        return new SpecifiedPlacement();
    }

    /**
     * @return бин, выполняющий размещение кораблей на игровом поле с помощью указанного алгоритма
     * {@link PlacementExecutor}
     */
    @Bean
    public PlacementExecutor placementExecutor() {
        // Получаем наименование алгоритма (из файла config.properties)
        final String algorithmName = environment.getProperty("placement.algorithm", "random");
        final PlacementAlgorithm algorithm;
        // Флаг, казывающий на то, что приложение запущено в режиме тестирования
        final boolean isTest = Stream.of(context.getEnvironment().getActiveProfiles()).anyMatch("test"::equals);
        // Если приложение запущено в режиме тестирования
        if (isTest) {
            // Используем предопределенное размещение кораблей
            algorithm = specifiedPlacementAlgorithm();
        } else {
            // Получаем бин, реализующий данный алгоритм
            if ("random".equals(algorithmName)) {
                algorithm = randomPlacementAlgorithm();
            } else if ("specified".equals(algorithmName)) {
                algorithm = specifiedPlacementAlgorithm();
            } else {
                // Выбрасываем исключение, если не удалось распознать алгоритм
                throw new BeanInitializationException("Unknown algorithm of placement: " + algorithmName + "!");
            }
        }
        // Создаем и возвращаем бин, выполняющий размещение кораблей с помощью указанного алгоритма
        return new PlacementExecutor(algorithm);
    }

    /**
     * @return палуба {@link Deck}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Deck deck() {
        return new Deck();
    }

    /**
     * @return однопалубный корабль {@link Ship}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Ship oneDeckShip() {
        final Ship ship = new Ship();
        ship.setDecks(deck());
        return ship;
    }

    /**
     * @return двухпалубный корабль {@link Ship}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Ship twoDecksShip() {
        final Ship ship = new Ship();
        ship.setDecks(deck(), deck());
        return ship;
    }

    /**
     * @return трехпалубный корабль {@link Ship}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Ship treeDecksShip() {
        final Ship ship = new Ship();
        ship.setDecks(deck(), deck(), deck());
        return ship;
    }

    /**
     * @return четырехпалубный корабль {@link Ship}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Ship fourDecksShip() {
        final Ship ship = new Ship();
        ship.setDecks(deck(), deck(), deck(), deck());
        return ship;
    }

    /**
     * Преобразователь индексов столбцов {@link ColumnIndexConverter}
     */
    @Bean
    public ColumnIndexConverter columnIndexConverter() {
        return new ColumnIndexConverter();
    }

    /**
     * @return ячейка игрового поля {@link Cell}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Cell cell() {
        return new Cell();
    }

    /**
     * @return снимок ячейки игрового поля {@link CellSnapshot}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public CellSnapshot cellSnapshot() {
        return new CellSnapshot();
    }

    /**
     * @return игровое поле размером 10x10 {@link Field}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public Field field() {
        // Хардкодим размерность поля
        final int size = 10;
        // Создаем массив для ячеек
        final Cell[][] cells = new Cell[size][size];
        // Заполняем массив ячеек
        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {
                // Создаем ячейку
                final Cell cell = cell();
                // Задаем ячейке номер строки
                cell.setRow(row);
                // Задаем ячейке номер столбца
                cell.setColumn(column);
                // Помещаем ячейку в массив
                cells[row][column] = cell;
            }
        }
        // Создаем поле
        final Field field = new Field();
        // Устанавливаем полю массив ячеек
        field.setCells(cells);
        // Возвращаем поле
        return field;
    }

    /**
     * @return снимок игрового поля {@link FieldSnapshot}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public FieldSnapshot fieldSnapshot() {
        return new FieldSnapshot();
    }

    /**
     * @return хранитель символов, необходимых для печати игрового поля {@link FieldPrinterSymbolsKeeper}
     */
    @Bean
    public FieldPrinterSymbolsKeeper fieldPrinterSymbolsKeeper() {
        // Получаем символы, необходимые для печати игрового поля (из файла config.properties)
        final String notAttacked = environment.getProperty("print.cell.not-attacked", "~");
        final String attacked = environment.getProperty("print.cell.attacked", "-");
        final String deck = environment.getProperty("print.cell.deck", "o");
        final String destroyedDeck = environment.getProperty("print.cell.destroyed-deck", "x");
        // Создаем и возвращаем бин
        return new FieldPrinterSymbolsKeeper(notAttacked, attacked, deck, destroyedDeck);
    }

    /**
     * @return принтер для печати игрового поля {@link FieldPrinter}
     */
    @Bean
    public FieldPrinter fieldPrinter() {
        return new FieldPrinter(fieldPrinterSymbolsKeeper());
    }

    /**
     * @return игрок 1 {@link Player}
     */
    @Bean
    public Player player1() {
        final Player player = new Player();
        player.setName("player1");
        return player;
    }

    /**
     * @return игрок 2 {@link Player}
     */
    @Bean
    public Player player2() {
        final Player player = new Player();
        player.setName("player2");
        return player;
    }

    /**
     * @return снимок информации об игроке {@link PlayerSnapshot}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public PlayerSnapshot playerSnapshot() {
        return new PlayerSnapshot();
    }

    /**
     * @return игра для игроков player1 и player2 {@link Game}
     */
    @Bean
    public Game game() {
        return new Game(player1(), player2(), placementExecutor(), columnIndexConverter(), fieldPrinter());
    }

    /**
     * @return снимок информации об игре {@link GameSnapshot}
     */
    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public GameSnapshot gameSnapshot() {
        return new GameSnapshot();
    }
}
