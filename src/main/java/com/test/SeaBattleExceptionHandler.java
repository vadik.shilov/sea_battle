package com.test;

import com.test.entity.game.BadCellIndexException;
import com.test.entity.game.IllegalActionException;
import com.test.entity.game.MoveOutOfTurnException;
import com.test.entity.game.UnknownPlayerException;
import com.test.entity.game.player.field.BadCoordinateException;
import com.test.entity.game.player.field.CellAlreadyAttackedException;
import com.test.entity.game.player.field.converter.BadColumnAliasException;
import com.test.entity.game.player.field.converter.ColumnIndexConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Обработчик ошибок
 */
@ControllerAdvice
@PropertySource("classpath:localize/error/error.properties")
public class SeaBattleExceptionHandler {
    @Autowired
    private Environment environment; // Локализация сообщений об ошибках
    @Autowired
    private ColumnIndexConverter columnIndexConverter;

    /**
     * Обработка ошибок типа {@link UnknownPlayerException}
     *
     * @param exception ошибка {@link UnknownPlayerException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onUnknownPlayerException(UnknownPlayerException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty("unknown-player", "Unknown player: %s!");
        // Получаем отформатированное текстовое сообщение об ошибке
        final String errorMessage = formatMessage(errorTemplate, exception.getName());
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.UNAUTHORIZED.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link BadCellIndexException}
     *
     * @param exception ошибка {@link BadCellIndexException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onBadCellIndexException(BadCellIndexException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty("bad-cell-index", "Bad cell index: %s!");
        // Получаем отформатированное текстовое сообщение об шибке
        final String errorMessage = formatMessage(errorTemplate, exception.getCellIndex());
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link BadCoordinateException}
     *
     * @param exception ошибка {@link BadCoordinateException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onBadCoordinateException(BadCoordinateException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty("bad-coordinate", "Bad coordinate: %s!");
        // Получаем отформатированное текстовое сообщение об шибке
        final String errorMessage = formatMessage(
                errorTemplate,
                exception.isRow() ?
                        exception.getCoordinate() + 1 :
                        columnIndexConverter.getAlias(exception.getCoordinate())
        );
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link BadColumnAliasException}
     *
     * @param exception ошибка {@link BadColumnAliasException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onBadColumnAliasException(BadColumnAliasException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty("bad-column-alias", "Bad column alias: %s!");
        // Получаем отформатированное текстовое сообщение об шибке
        final String errorMessage = formatMessage(errorTemplate, exception.getAlias());
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link CellAlreadyAttackedException}
     *
     * @param exception ошибка {@link CellAlreadyAttackedException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onCellAlreadyAttackedException(CellAlreadyAttackedException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty(
                "cell-already-attacked",
                "The cell [%s, %s] already attacked!"
        );
        // Получаем отформатированное текстовое сообщение об ошибке
        final String errorMessage = formatMessage(
                errorTemplate,
                exception.getRow() + 1,
                columnIndexConverter.getAlias(exception.getColumn())
        );
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link IllegalActionException}
     *
     * @param response http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler(IllegalActionException.class)
    public void onIllegalActionException(HttpServletResponse response)
            throws IOException {
        // Получаем текст сообщения об ошибке
        final String errorMessage = environment.getProperty("illegal-action", "Illegal action!");
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Обработка ошибок типа {@link MoveOutOfTurnException}
     *
     * @param exception ошибка {@link MoveOutOfTurnException}
     * @param response  http-ответ {@link HttpServletResponse}
     */
    @ExceptionHandler
    public void onMoveOutOfTurnException(MoveOutOfTurnException exception, HttpServletResponse response)
            throws IOException {
        // Получаем текст шаблона сообщения об ошибке
        final String errorTemplate = environment.getProperty("move-out-of-turn", "%s tried to move out of turn!");
        // Получаем отформатированное текстовое сообщение об ошибке
        final String errorMessage = formatMessage(errorTemplate, exception.getPlayerName());
        // Отправляем ответ с сообщением об ошибке
        response.sendError(HttpStatus.BAD_REQUEST.value(), errorMessage);
    }

    /**
     * Отформатировать сообщение
     *
     * @param errorTemplate шаблон сообщения
     * @param args          массив аргументов
     * @return отформотированное сообщение
     */
    private String formatMessage(String errorTemplate, Object... args) {
        try {
            // Форматируем и возвращаем сообщение
            return String.format(errorTemplate, args);
        } catch (Exception ignore) {
            //TODO: В случае ошибки форматирования возвращаем пустую строку?
            return "";
        }
    }

    /*
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public VndErrors onUnknownPlayerExceptionException(UnknownPlayerException exception) {
        return new VndErrors(
                new VndErrors.VndError(
                        exception.getClass().getName(),
                        exception.getMessage()
                )
        );
    }
    */
}
