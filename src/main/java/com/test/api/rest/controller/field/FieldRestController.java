package com.test.api.rest.controller.field;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.test.entity.game.Game;
import com.test.entity.game.player.field.FieldSnapshot;
import com.test.entity.game.player.field.cell.CellSnapshot;
import com.test.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(
        value = "/game/players/",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
@PropertySource("classpath:localize/message/message.properties")
public class FieldRestController {
    @Autowired
    private Environment environment; // Сообщения
    @Autowired
    private GameService service; // Игровой сервис

    /**
     * Получить поле игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, поле которого необходимо получить
     * @return снимок игрового поля {@link FieldSnapshot}
     */
    @RequestMapping(value = {"/{targetPlayerName}/field"}, method = RequestMethod.GET)
    public ResponseEntity<FieldSnapshot> getField(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName
    ) {
        // Получаем информацию об игроке
        final FieldSnapshot fieldSnapshot = service.getField(requestPlayerName, targetPlayerName);
        // Если у игрока игровое поле задано
        if (fieldSnapshot != null) {
            // Возвращаем успешный ответ
            return ResponseEntity.ok(fieldSnapshot);
        }
        // Поле не найдено
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Получить список ячеек поля игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, список ячеек поля которого необходимо получить
     * @return список ячеек {@link CellSnapshot}
     */
    @RequestMapping(value = {"/{targetPlayerName}/field/cells"}, method = RequestMethod.GET)
    public ResponseEntity<List<CellSnapshot>> getCells(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName
    ) {
        // Получаем снимок поля игрока
        final FieldSnapshot fieldSnapshot = service.getField(requestPlayerName, targetPlayerName);
        // Если поле задано
        if (fieldSnapshot != null) {
            // Создаем список
            List<CellSnapshot> result = new LinkedList<>();
            // Перебираем строки массива с ячейками
            for (CellSnapshot[] rows : fieldSnapshot.getCells()) {
                // Кладем все ячейки из строки в список
                Collections.addAll(result, rows);
            }
            // Возвращаем успешный ответ
            return ResponseEntity.ok(result);
        }
        // Поле не найдено
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Получить снимок ячейки игрового поля
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, ячейку поля которого необходимо получить
     * @param cellIndex         индекс ячейки
     * @return снимок ячейки игрового поля
     */
    @RequestMapping(
            value = {"/{targetPlayerName}/field/cells/{cellIndex:[1-9][abcdefghij]|10[abcdefghij]}"},
            method = RequestMethod.GET
    )
    public ResponseEntity<CellSnapshot> getCell(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName,
            @PathVariable("cellIndex") String cellIndex
    ) {
        // Получаем снимок ячейки
        final CellSnapshot cellSnapshot = service.getCell(requestPlayerName, targetPlayerName, cellIndex);
        // Если ячейка получена
        if (cellSnapshot != null) {
            // Возвращаем успешный ответ
            return ResponseEntity.ok(cellSnapshot);
        }
        // Ячейка не найдена
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Совершить выстрел в ячейку игрового поля
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, статус ячееки поля которого необходимо изменить
     * @param cellIndex         индекс ячейки
     * @return сообщение с описанием резултата
     */
    @RequestMapping(
            value = {"/{targetPlayerName}/field/cells/{cellIndex:[1-9][abcdefghij]|10[abcdefghij]}"},
            method = RequestMethod.PUT
    )
    public String attackCell(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName,
            @PathVariable("cellIndex") String cellIndex
    ) {
        // Получаем результат
        final Game.TurnResult result = service.attackCell(requestPlayerName, targetPlayerName, cellIndex);

        // Текст сообщения
        final String message;
        // Определяем текст сообщения по результату
        if (result == Game.TurnResult.DESTROY_DECK) {
            // Попал!
            message = environment.getProperty("destroy-deck");
        } else if (result == Game.TurnResult.DESTROY_SHIP) {
            // Уничтожил!
            message = environment.getProperty("destroy-ship");
        } else if (result == Game.TurnResult.WIN) {
            // Победил!
            message = environment.getProperty("win");
        } else {
            // Мимо!
            message = environment.getProperty("miss");
        }

        // Создаем и возвращаем тело ответа
        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode root = mapper.createObjectNode();
        root.put("result", result.name());
        root.put("message", message);
        try {
            return mapper.writeValueAsString(root);
        } catch (JsonProcessingException e) {
            // Выбрасываем исключение, если при маршалинге произошла ошибка
            throw new IllegalStateException();
        }
    }
}
