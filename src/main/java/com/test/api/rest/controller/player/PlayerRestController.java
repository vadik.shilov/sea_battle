package com.test.api.rest.controller.player;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.test.entity.game.GameSnapshot;
import com.test.entity.game.player.Player;
import com.test.entity.game.player.PlayerSnapshot;
import com.test.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(
        value = "/game/players",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class PlayerRestController {
    @Autowired
    private GameService service; // Игровой сервис

    /**
     * Получить информацию об игроках
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @return список игроков {@link PlayerSnapshot}
     */
    @RequestMapping(method = RequestMethod.GET)
    public List<PlayerSnapshot> getPlayers(@RequestHeader("player-name") String requestPlayerName) {
        // Получаем информацию об игре
        final GameSnapshot gameSnapshot = service.getGameInfo(requestPlayerName);
        // Создаем и возвращаем список информации об игроках
        return Arrays.asList(gameSnapshot.getPlayer1(), gameSnapshot.getPlayer2());
    }

    /**
     * Получить информацию об игроке
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, информацию о котором необходимо получить
     * @return информация об игроке {@link PlayerSnapshot}
     */
    @RequestMapping(value = {"/{targetPlayerName}"}, method = RequestMethod.GET)
    public PlayerSnapshot getPlayer(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName
    ) {
        // Получаем информацию об игроке
        return service.getPlayerInfo(requestPlayerName, targetPlayerName);
    }

    /**
     * Получить статус игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, статус которого необходимо получить
     * @return статус игрока
     * @see Player.State
     */
    @RequestMapping(value = {"/{targetPlayerName}/state"}, method = RequestMethod.GET)
    public String getPlayerState(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName
    ) {
        // Получаем статус игрока
        final Player.State state = service.getPlayerState(requestPlayerName, targetPlayerName);
        // Создаем и возвращаем тело ответа
        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode root = mapper.createObjectNode();
        root.put("state", state.name());
        try {
            return mapper.writeValueAsString(root);
        } catch (JsonProcessingException e) {
            // Выбрасываем исключение, если при маршалинге произошла ошибка
            throw new IllegalStateException();
        }
    }

    /**
     * Изменить статус игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока (указано в заголовке "player-name" запроса)
     * @param targetPlayerName  наименование игрока, статус которого необходимо изменить
     * @param newPlayerState    новый статус игрока (из тела запроса)
     * @return статус игрока
     * @see Player.State
     */
    @RequestMapping(value = {"/{targetPlayerName}/state"}, method = RequestMethod.PUT)
    public ResponseEntity<?> changePlayerState(
            @RequestHeader("player-name") String requestPlayerName,
            @PathVariable("targetPlayerName") String targetPlayerName,
            @RequestBody ObjectNode newPlayerState
    ) {
        // Если тело запроса выглядет так: {"state": "статус"}
        if (newPlayerState.size() == 1 && newPlayerState.has("state")) {
            // Новый статус игрока
            final Player.State state;
            try {
                // Парсим статус
                state = Player.State.valueOf(newPlayerState.get("state").asText());
            } catch (Exception e) {
                // Возвращаем ошибку 400
                return ResponseEntity.badRequest().build();
            }
            // Пробуем изменить статус игроку
            service.changePlayerState(requestPlayerName, targetPlayerName, state);
            // Возвращаем успешный ответ
            return ResponseEntity.ok().build();
        }
        // Возвращаем ошибку 400
        return ResponseEntity.badRequest().build();
    }
}
