package com.test.api.rest.controller.game;

import com.test.entity.game.GameSnapshot;
import com.test.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        value = "/game",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class GameRestController {
    @Autowired
    private GameService service; // Игровой сервис

    /**
     * Получить информацию об игре
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @return информация об игре
     * @see GameSnapshot
     */
    @RequestMapping(method = RequestMethod.GET)
    public GameSnapshot getGameInfo(@RequestHeader("player-name") String requestPlayerName) {
        // Возвращаем информацию об игре
        return service.getGameInfo(requestPlayerName);
    }
}
