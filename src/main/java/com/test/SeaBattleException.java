package com.test;

/**
 * Ошибка игры Sea Battle
 */
public abstract class SeaBattleException extends RuntimeException {
    /**
     * @param message сообщение об ошибке
     * @param cause причиниа
     */
    public SeaBattleException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message сообщение об ошибке
     */
    public SeaBattleException(String message) {
        super(message);
    }

    /**
     * @param cause причиниа
     */
    public SeaBattleException(Throwable cause) {
        super(cause);
    }

    public SeaBattleException() {
    }
}
