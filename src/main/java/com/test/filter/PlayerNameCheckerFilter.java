package com.test.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Фильтр выполняющий проверку того, что в запросе содержится заголовок с наименованием игрока
 */
@PropertySource("classpath:localize/error/error.properties")
public class PlayerNameCheckerFilter extends GenericFilterBean {
    private static final String HEADER_NAME = "player-name"; // Наименование заголовока
    @Value("${player-name-is-not-set}")
    private String playerNameIsNotSetErrorText; // Сообщение об ошибке

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // Получаем наименование игрока из заголовка
        final String playerName = ((HttpServletRequest) request).getHeader(HEADER_NAME);
        // Если наименование не указано
        if (StringUtils.isEmpty(playerName)) {
            // Возвращаем ошибку авторизации
            ((HttpServletResponse) response).sendError(
                    HttpServletResponse.SC_UNAUTHORIZED,
                    playerNameIsNotSetErrorText
            );
            return;
        }
        // Передаем запрос дальше по цепи ответственности
        chain.doFilter(request, response);
    }
}
