package com.test.service;

import com.test.entity.game.Game;
import com.test.entity.game.GameSnapshot;
import com.test.entity.game.player.Player;
import com.test.entity.game.player.PlayerSnapshot;
import com.test.entity.game.player.field.FieldSnapshot;
import com.test.entity.game.player.field.cell.CellSnapshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {
    @Autowired
    private Game game; // Игра

    /**
     * Получить информацию об игре
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @return снимок информации об игре {@link GameSnapshot}
     */
    public GameSnapshot getGameInfo(String requestPlayerName) {
        // Получаем и возвращаем снимок информации об игре
        return game.getSnapshot(requestPlayerName);
    }

    /**
     * Получить информацию об игроке
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, информацию о котором необходимо получить
     * @return снимок информации об игроке {@link PlayerSnapshot}
     */
    public PlayerSnapshot getPlayerInfo(String requestPlayerName, String targetPlayerName) {
        // Получаем и возвращаем снимок информации об игроке
        return game.getPlayerSnapshot(requestPlayerName, targetPlayerName);
    }

    /**
     * Получить статус игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, статус которого необходимо получить
     * @return статус игрока {@link Player.State}
     */
    public Player.State getPlayerState(String requestPlayerName, String targetPlayerName) {
        // Получаем и возвращаем статус игрока
        return game.getPlayerState(requestPlayerName, targetPlayerName);
    }

    /**
     * Изменить статус игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, статус  которого необходимо изменить
     * @param newPlayerState    новый статус игрока {@link Player.State}
     */
    public void changePlayerState(String requestPlayerName, String targetPlayerName, Player.State newPlayerState) {
        // Изменяем статус игрока
        game.changePlayerState(requestPlayerName, targetPlayerName, newPlayerState);
    }

    /**
     * Получить поле игрока
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, поле которого необходимо получить
     * @return снимок игрового поля {@link FieldSnapshot}
     */
    public FieldSnapshot getField(String requestPlayerName, String targetPlayerName) {
        // Получаем и возвращаем снимок поля
        return game.getFieldSnapshot(requestPlayerName, targetPlayerName);
    }

    /**
     * Получить ячейку игрового поля
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, выстрел в поле которого необходимо совершить
     * @param cellIndex         индекс ячейки
     * @return снимок ячейки игрового поля {@link CellSnapshot}
     */
    public CellSnapshot getCell(String requestPlayerName, String targetPlayerName, String cellIndex) {
        // Получаем и возвращаем снимок ячейки
        return game.getCellSnapshot(requestPlayerName, targetPlayerName, cellIndex);
    }

    /**
     * Совершить выстрел в ячейку игрового поля
     *
     * @param requestPlayerName наименование выполнившего запрос игрока
     * @param targetPlayerName  наименование игрока, выстрел в поле которого необходимо совершить
     * @param cellIndex         индекс ячейки
     * @return результат выстрела {@link Game.TurnResult}
     */
    public Game.TurnResult attackCell(String requestPlayerName, String targetPlayerName, String cellIndex) {
        // Получаем и возвращаем результат выстрела
        return game.turn(requestPlayerName, targetPlayerName, cellIndex);
    }
}